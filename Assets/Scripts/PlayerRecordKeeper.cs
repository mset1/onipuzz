﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class PlayerRecordKeeper : MonoBehaviour {
    private static string SAVE_DIRECTORY = Application.persistentDataPath;
    private static string JSON_SAVE_NAME = "/playerData.json";

    [SerializeField]
    private static PlayerRecordCollection recordCollection = new PlayerRecordCollection();
    [System.Serializable]
    private class PlayerRecordCollection {
        public List<PlayerRecord> records = new List<PlayerRecord>();
    }

    public static void Save(PlayerRecord playerRecord) {
        if (!ReplaceOldEntry(playerRecord)) {
            recordCollection.records.Add(playerRecord);
        }
        WriteRecordsToJSON();
    }
    public static void Load() {
        if (File.Exists(SAVE_DIRECTORY + JSON_SAVE_NAME)) {
            ReadRecordsFromJSON();
        }
    }
    public static PlayerRecord GetPlayerRecord(int boardId) {
        foreach (PlayerRecord record in recordCollection.records) { 
            if (boardId == record.boardId) {
                return record;
            }
        }
        return null;
    }

    private static bool ReplaceOldEntry(PlayerRecord record) {
        int oldIndex = -1;
        for (int i = 0; i < recordCollection.records.Count; i++) {
            if (recordCollection.records[i].boardId == record.boardId) {
                oldIndex = i;
            }
        }
        if (oldIndex >= 0) {
            if (recordCollection.records[oldIndex].bestMoves > record.bestMoves) {
                recordCollection.records[oldIndex] = record;
            }
            return true;
        }
        return false;
    }

    private static void ReadRecordsFromJSON() {
        string jsonText = File.ReadAllText(SAVE_DIRECTORY + JSON_SAVE_NAME);
        recordCollection = JsonUtility.FromJson<PlayerRecordCollection>(jsonText);
    }
    private static void WriteRecordsToJSON() {
        string recordsJson = JsonUtility.ToJson(recordCollection);
        System.IO.File.WriteAllText(SAVE_DIRECTORY + JSON_SAVE_NAME, recordsJson);
    }
}
