﻿using UnityEngine;
using System;
using System.Collections;

public class AudioManager : MonoBehaviour {
    public static AudioManager instance = null;
    public Sound[] sfxArray;
    public Sound[] musicArray;
    private static SoundID currentTrack = SoundID.NONE;
    private static float masterMultiplierVolume = 1;
    private static float musicMultiplierVolume = 1;
    private static float sfxMultiplierVolume = 1;

    private const string masterPrefString = "masterVolume";
    private const string musicPrefString = "musicVolume";
    private const string sfxPrefString = "sfxVolume";

    public enum SoundID {
        ROTATE_CW, ROTATE_CCW, 
        VICTORY_JINGLE, PERFECT_JINGLE,
        TRACK, NONE
    }
    public static void PlayButtonSound() {
        AudioManager.PlaySFX(SoundID.ROTATE_CW, 1f);
    }
    public static void PlayInsertSound() {
        AudioManager.PlaySFX(SoundID.ROTATE_CCW, 2.5f);
    }
    public static void PlayVictorySound() {
        AudioManager.PlaySFX(SoundID.VICTORY_JINGLE, 1f);
    }
    public static void PlayPerfectSound() {
        AudioManager.PlaySFX(SoundID.PERFECT_JINGLE, 1f);
    }
    private void Awake() {
        if (AudioManager.instance != null) { return; }

        Debug.Log("Awakening");
        this.CreateNewAudioManager();
        foreach (Sound sound in sfxArray) {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
        }
        foreach (Sound sound in musicArray) {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
        }
        AudioManager.PlayTrack(SoundID.TRACK);
        if (PlayerPrefs.HasKey(masterPrefString) &&
            PlayerPrefs.HasKey(musicPrefString) &&
            PlayerPrefs.HasKey(sfxPrefString)) {
            masterMultiplierVolume = PlayerPrefs.GetFloat(masterPrefString);
            musicMultiplierVolume = PlayerPrefs.GetFloat(musicPrefString);
            sfxMultiplierVolume = PlayerPrefs.GetFloat(sfxPrefString);
            SetMasterVolume(masterMultiplierVolume);
        } else {
            masterMultiplierVolume = 1;
            musicMultiplierVolume = 1;
            sfxMultiplierVolume = 1;
            SetMasterVolume(masterMultiplierVolume);
        }
    }
    private void CreateNewAudioManager() {
        GameObject[] audioManagerObjects = GameObject.FindGameObjectsWithTag("AudioManager");
        if (audioManagerObjects.Length > 1) {
            Debug.Log("Dupe");
            Destroy(this.gameObject);
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    public static void PlayTrack(SoundID id) {
        Sound s = FindTrackWithID(id);
        if (s == null) {
            Debug.Log("Id:" + id + " not found");
            return;
        }
        AudioManager.currentTrack = id;
        s.source.volume = s.volume * AudioManager.musicMultiplierVolume;
        s.source.Play();
        s.source.loop = true;
    }
    private static void PlaySFX(SoundID id, float pitch) {
        Sound s = FindSFXWithID(id);
        if (s == null) {
            Debug.Log("Id:" + id + " not found");
            return; }
        s.source.pitch = pitch;
        s.source.volume = s.volume 
            * AudioManager.sfxMultiplierVolume
            * AudioManager.masterMultiplierVolume;
        s.source.PlayOneShot(s.clip);
    }
    public static void PlaySFXRandomPitch(SoundID id) {
        Sound s = FindSFXWithID(id);
        if (s == null) { return; }
        s.source.pitch = UnityEngine.Random.Range(s.randomPitchMin, s.randomPitchMax);
        s.source.volume = s.volume 
            * AudioManager.sfxMultiplierVolume
            * AudioManager.masterMultiplierVolume;
        s.source.PlayOneShot(s.clip);
    }
    private static Sound FindTrackWithID(SoundID id) {
        foreach (Sound sound in instance.musicArray) {
            if (sound.soundId == id) {
                return sound;
            }
        }
        return null;
    }
    public static void SetMasterVolume(float volume) {
        AudioManager.masterMultiplierVolume = volume;
        PlayerPrefs.SetFloat(masterPrefString, volume);
        AudioManager.SetMusicVolume(AudioManager.musicMultiplierVolume);
        AudioManager.SetSFXVolume(AudioManager.sfxMultiplierVolume);
    }
    public static void SetMusicVolume(float volume) {
        AudioManager.musicMultiplierVolume = volume;
        PlayerPrefs.SetFloat(musicPrefString, volume);
        if (AudioManager.currentTrack != SoundID.NONE) {
            Sound s = FindTrackWithID(AudioManager.currentTrack);
            s.source.volume = s.volume 
                * AudioManager.musicMultiplierVolume
                * AudioManager.masterMultiplierVolume;
        }
    }
    public static void SetSFXVolume(float volume) {
        AudioManager.sfxMultiplierVolume = volume;
        PlayerPrefs.SetFloat(sfxPrefString, volume);
    }
    private static Sound FindSFXWithID(SoundID id) {
        foreach (Sound sound in instance.sfxArray) {
            if (sound.soundId == id) {
                return sound;
            }
        }
        return null;
    }
    public static float GetMasterVolume() {
        return AudioManager.masterMultiplierVolume;
    }
    public static float GetMusicVolume() {
        return AudioManager.musicMultiplierVolume;
    }
    public static float GetSFXVolume() {
        return AudioManager.sfxMultiplierVolume;
    }
}
