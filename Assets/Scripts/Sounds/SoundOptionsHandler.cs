﻿using UnityEngine;
using System.Collections;

public class SoundOptionsHandler : MonoBehaviour {
    public UnityEngine.UI.Slider masterSlider;
    public UnityEngine.UI.Slider sfxSlider;
    public UnityEngine.UI.Slider musicSlider;

    private float masterVolume = 1;
    private float sfxVolume = 1;
    private float musicVolume = 1;

    public void Awake() {
        this.masterSlider = GameObject.Find("MasterSlider").GetComponent<UnityEngine.UI.Slider>();
        this.sfxSlider = GameObject.Find("SFXSlider").GetComponent<UnityEngine.UI.Slider>();
        this.musicSlider = GameObject.Find("MusicSlider").GetComponent<UnityEngine.UI.Slider>();

        this.masterSlider.value = AudioManager.GetMasterVolume();
        this.musicSlider.value = AudioManager.GetMusicVolume();
        this.sfxSlider.value = AudioManager.GetSFXVolume();
    }
    public void SetMasterVolume(float volume) {
        this.masterVolume = volume;
        AudioManager.SetMasterVolume(volume);
    }
    public void SetSFXVolume(float volume) {
        this.sfxVolume = volume;
        AudioManager.SetSFXVolume(this.masterVolume * this.sfxVolume);
    }
    public void SetMusicVolume(float volume) {
        this.musicVolume = volume;
        AudioManager.SetMusicVolume(this.masterVolume * this.musicVolume);
    }
}
