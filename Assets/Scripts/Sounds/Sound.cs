﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Sound : MonoBehaviour {
    public AudioManager.SoundID soundId;
    public AudioClip clip;
    [HideInInspector]
    public AudioSource source;

    [Range(0f, 1f)]
    public float volume = 0.5f;
    [Range(0.1f, 3f)]
    public float pitch = 1f;
    public float randomPitchMin = 0.8f;
    public float randomPitchMax = 1.2f;
}
