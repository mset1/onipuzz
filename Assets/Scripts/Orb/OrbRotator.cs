﻿using UnityEngine;
using System.Collections;

public class OrbRotator : MonoBehaviour {
    public GameObject RotatorPinPrefab;
    private OniPuzzleGame oniPuzzleGame;
    private OrbBoard board;
    private Vector2 startingPosition = new Vector2(0f, 0f);
    private RotatorPin[,] rotatorPins;
    public int width;
    public int height;
    public int currentX = 0;
    public int currentY = 0;

    private OrbSlot[] rotatingSlots;
    private const float ROTATION_MAX = 90f;
    private const float ROTATION_SPEED = 1000f;
    private int rotationDirection = -1;
    private float currentRotation = 0;
    private bool isRotating = false;

    public void Update() {
        RotateOrbs();
    }
    public void SetGame(OniPuzzleGame game) {
        this.oniPuzzleGame = game;
    }
    public void SetBoard(OrbBoard board) {
        this.board = board;
        this.SetBoardDimensions(board.width, board.height);
        this.startingPosition = board.transform.position;
        this.startingPosition -= Vector2.right * (Orb.SIZE * ((this.width-1) / 2f));
        this.startingPosition -= Vector2.up * (Orb.SIZE * ((this.height-1) / 2f));
        this.transform.position = this.startingPosition;
        this.GeneratePins();
    }
    public void RotateCWAtPosition() {
        if (this.isRotating) { return; }
        AudioManager.PlaySFXRandomPitch(AudioManager.SoundID.ROTATE_CW);
        OrbSlot[] slots = this.board.GetSlotsAtPosition(this.currentX, this.currentY);
        this.InitiateRotation(slots, true);
    }
    public void RotateCCWAtPosition() {
        if (this.isRotating) { return; }
        AudioManager.PlaySFXRandomPitch(AudioManager.SoundID.ROTATE_CCW);
        OrbSlot[] slots = this.board.GetSlotsAtPosition(this.currentX, this.currentY);
        this.InitiateRotation(slots, false);
    }
    public void InitiateRotation(OrbSlot[] orbSlots, bool isCWRotate) {
        this.rotatingSlots = orbSlots;
        this.isRotating = true;
        this.currentRotation = 0f;
        if (isCWRotate) { this.rotationDirection = -1; } 
        else { this.rotationDirection = 1; }
    }
    public void MoveRotator(int x, int y) {
        if (this.isRotating) { return; }
        int newX = x + this.currentX;
        if (newX >= this.width) { newX = this.width - 1; }
        else if (newX < 0) { newX = 0; }
        int newY = y + this.currentY;
        if (newY >= this.height) { newY = this.height - 1; }
        else if (newY < 0) { newY = 0; }

        this.rotatorPins[newX, newY].SetRotatorPosition();
    }
    public void MoveRotatorToPin(Collider2D collider) {
        if (this.isRotating) { return; }
        foreach (RotatorPin pin in this.rotatorPins) {
            if (pin.pinCollider == collider) {
                pin.SetRotatorPosition();
                return;
            }
        }
    }

    private void RotateOrbs() {
        if (this.rotatingSlots == null || !this.isRotating) { return; }
        if (Mathf.Abs(this.currentRotation) < Mathf.Abs(ROTATION_MAX)) {
            float rotationIncrement = this.rotationDirection * ROTATION_SPEED * Time.deltaTime;
            this.currentRotation += rotationIncrement;
            foreach (OrbSlot slot in this.rotatingSlots) {
                Transform orbTransform = slot.orb.transform;
                orbTransform.RotateAround(
                    transform.position, Vector3.forward, rotationIncrement);
                orbTransform.rotation = slot.transform.rotation;
            }
            this.transform.Rotate(Vector3.forward, rotationIncrement);
        } else {
            this.isRotating = false;
            this.rotatingSlots = null;
            if (this.rotationDirection == -1) {
                this.board.RotateOrbsCW(this.currentX, this.currentY);
                this.oniPuzzleGame.IncrementMoves(1);
            } else {
                this.board.RotateOrbsCCW(this.currentX, this.currentY);
                this.oniPuzzleGame.IncrementMoves(1);
            }
            this.transform.rotation = this.board.transform.rotation;
        }
    }
    private void SetBoardDimensions(int width, int height) {
        this.width = width - 1;
        this.height = height - 1;
    }
    private void GeneratePins() {
        this.rotatorPins = new RotatorPin[this.width, this.height];
        for (int row = 0; row < this.height; row++) {
            for (int col = 0; col < this.width; col++) {
                Vector3 pinPosition = this.startingPosition + new Vector2(col * Orb.SIZE, row * Orb.SIZE);
                this.rotatorPins[col, row] = CreateRotatorPin(pinPosition);
                this.rotatorPins[col, row].SetPosition(col, row);
            }
        }
    }
    private RotatorPin CreateRotatorPin(Vector3 worldPosition) {
        GameObject pinObject = (GameObject)Instantiate(this.RotatorPinPrefab, worldPosition, this.transform.rotation);
        pinObject.transform.parent = this.board.transform;
        RotatorPin pin = pinObject.GetComponent<RotatorPin>();
        pin.rotator = this;
        return pin;
    }
    public RotatorPin[,] GetRotatorPins() {
        return this.rotatorPins;
    }
}
