﻿using UnityEngine;
using System.Collections;

public class OrbBoard : MonoBehaviour {
    private Orb[] orbs;
    public OrbGoal[,] orbGoals;
    public OrbSlot[,] orbSlots;
    public OrbRotator rotator;
    public BoardParameters parameters;
    public int width = 3;
    public int height = 2;

    public void Initialize() {
        OrbBoardInitializer initializer = this.GetComponent<OrbBoardInitializer>();
        initializer.SetDimensions(this.width, this.height);
        Vector2 startingPosition = this.transform.position;
        startingPosition -= Vector2.right * (Orb.SIZE * (this.width / 2f - 0.5f));
        startingPosition -= Vector2.up * (Orb.SIZE * (this.height / 2f - 0.5f));
        this.orbSlots = initializer.GenerateOrbSlots(startingPosition);
        this.orbs = initializer.GenerateOrbs(this.orbSlots);
        this.orbGoals = initializer.GenerateDefaultOrbGoals(startingPosition);
        initializer.SetDefaultColors(this.orbGoals, this.orbs);

        Transform rotatorObject = this.transform.Find("Rotator");
        this.rotator = rotatorObject.GetComponent<OrbRotator>();
        this.rotator.SetBoard(this);
    }
    public void RotateOrbsCCW(int positionX, int positionY) {
        if (positionX >= this.width -1) { positionX = this.width - 1; }
        if (positionY >= this.height - 1) { positionY = this.height - 1; }
        Orb tempOrb = this.orbSlots[positionX, positionY].orb;
        this.AssignOrbToSlot(this.orbSlots[positionX, positionY], this.orbSlots[positionX, positionY + 1].orb);
        this.AssignOrbToSlot(this.orbSlots[positionX, positionY+1], this.orbSlots[positionX + 1, positionY + 1].orb);
        this.AssignOrbToSlot(this.orbSlots[positionX+1, positionY+1], this.orbSlots[positionX + 1, positionY].orb);
        this.AssignOrbToSlot(this.orbSlots[positionX+1, positionY], tempOrb);
    }
    public void RotateOrbsCW(int positionX, int positionY) {
        if (positionX >= this.width - 1) { positionX = this.width - 1; }
        if (positionY >= this.height - 1) { positionY = this.height - 1; }
        Orb tempOrb = this.orbSlots[positionX, positionY].orb;
        this.AssignOrbToSlot(this.orbSlots[positionX, positionY], this.orbSlots[positionX+1, positionY].orb);
        this.AssignOrbToSlot(this.orbSlots[positionX+1, positionY], this.orbSlots[positionX + 1, positionY + 1].orb);
        this.AssignOrbToSlot(this.orbSlots[positionX + 1, positionY + 1], this.orbSlots[positionX, positionY+1].orb);
        this.AssignOrbToSlot(this.orbSlots[positionX, positionY+1], tempOrb);
    }
    public OrbSlot[] GetSlotsAtPosition(int positionX, int positionY) {
        OrbSlot[] slots = new OrbSlot[4];
        slots[0] = this.orbSlots[positionX, positionY];
        slots[1] = this.orbSlots[positionX + 1, positionY];
        slots[2] = this.orbSlots[positionX + 1, positionY + 1];
        slots[3] = this.orbSlots[positionX, positionY + 1];
        return slots;
    }
    public bool CheckBoardMatch() {
        bool matched = true;
        for (int row = 0; row < this.height; row++) {
            int type = this.orbGoals[0, row].type;
            OrbSlot[] slotRow = this.GetOrbSlotRow(row);
            bool rowMatch = this.CheckRowMatch(slotRow, type);
            EffectsHandler.SetRowActiveStatus(row, rowMatch);
            this.ActivateGoal(row, rowMatch);
            if (!rowMatch) { matched = false; }
        }
        return matched;
    }
    private bool CheckRowMatch(OrbSlot[] slotRow, int type) {
        foreach (OrbSlot slot in slotRow) {
            if (slot.orb.type != type) {
                return false;
            }
        }
        return true;
    }
    private OrbSlot[] GetOrbSlotRow(int row) {
        OrbSlot[] slotRow = new OrbSlot[this.width];
        for (int col = 0; col < this.width; col++) {
            slotRow[col] = this.orbSlots[col, row];
        }
        return slotRow;
    }
    private void ActivateGoal(int row, bool isActive) {
        this.orbGoals[0, row].SetActive(isActive);
        this.orbGoals[1, row].SetActive(isActive);
    }
    private void AssignOrbToSlot(OrbSlot orbSlot, Orb orb) {
        orbSlot.orb = orb;
        orbSlot.SetOrbPosition(orb);
    }
    public void SetDimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }
    private void SetOrbColors(int[,] colors) {
        for (int row = 0; row < this.orbSlots.GetLength(1); row++) {
            for (int col = 0; col < this.orbSlots.GetLength(0); col++) {
                this.orbSlots[col, row].orb.setColor(colors[col, row]);
            }
        }
    }
    public int[,] GetOrbColors() {
        int[,] colors = new int[this.width, this.height];
        for (int row = 0; row < this.orbSlots.GetLength(1); row++) {
            for (int col = 0; col < this.orbSlots.GetLength(0); col++) {
                colors[col, row] = this.orbSlots[col, row].orb.type;
            }
        }
        return colors;
    }

    public void GenerateBoardParameters() {
        this.parameters = new BoardParameters();
        this.parameters.width = this.width;
        this.parameters.height = this.height;
        this.SaveBoardState();
    }
    public void LoadBoardParameters(BoardParameters parameters) {
        this.SetBoardColors(parameters.orbColors, parameters.goalColors);
        this.GenerateBoardParameters();
        this.parameters.id = parameters.id;
        this.parameters.parMoves = parameters.parMoves;
        this.parameters.packId = parameters.packId;
        this.parameters.levelId = parameters.levelId;
    }
    public void SaveBoardState() {
        this.parameters.orbColors = new int[this.width * this.height];
        this.parameters.goalColors = new int[this.height];
        for (int row = 0; row < this.orbSlots.GetLength(1); row++) {
            for (int col = 0; col < this.orbSlots.GetLength(0); col++) {
                this.parameters.orbColors[col + row * this.width] = this.orbSlots[col, row].orb.type;
            }
            this.parameters.goalColors[row] = this.orbGoals[0, row].type;
        }
    }
    private void SetBoardColors(int[] orbColors, int[] goalColors) {
        for (int row = 0; row < this.height; row++) {
            for (int col = 0; col < this.width; col++) {
                this.orbSlots[col, row].orb.setColor(orbColors[row * width + col]);
            }
            this.orbGoals[0, row].setColor(goalColors[row]);
            this.orbGoals[1, row].setColor(goalColors[row]);
        }
    }
}
