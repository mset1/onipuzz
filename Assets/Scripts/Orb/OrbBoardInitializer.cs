﻿using UnityEngine;
using System.Collections;

public class OrbBoardInitializer : MonoBehaviour {
    public GameObject OrbSlotPrefab;
    public GameObject OrbPrefab;
    public GameObject OrbSettingPrefab;
    private int width = 2;
    private int height = 2;

    public SpriteRenderer[,] GetHaloSprites(OrbGoal[,] goals, OrbSlot[,] slots) {
        SpriteRenderer[,] halos = new SpriteRenderer[this.width + 2, this.height];
        for (int j = 0; j < this.height; j++) {
            Transform goalHaloTransform1 = goals[0, j].transform.Find("Halo");
            Transform goalHaloTransform2 = goals[1, j].transform.Find("Halo");
            halos[0, j] = goalHaloTransform1.GetComponent<SpriteRenderer>();
            halos[this.width+1, j] = goalHaloTransform2.GetComponent<SpriteRenderer>();
            for (int i = 0; i < this.width; i++) {
                Transform slotHaloTransform = slots[i, j].transform.Find("Halo");
                halos[i + 1, j] = slotHaloTransform.GetComponent<SpriteRenderer>();
            }
        }
        return halos;
    }
    public OrbSlot[,] GenerateOrbSlots(Vector2 startingPosition) {
        OrbSlot[,] orbSlots = new OrbSlot[this.width, this.height];
        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                Vector3 slotPosition = new Vector3(startingPosition.x + i * Orb.SIZE, startingPosition.y + j * Orb.SIZE, this.transform.position.z);
                orbSlots[i, j] = CreateOrbSlot(slotPosition);
            }
        }
        return orbSlots;
    }
    public Orb[] GenerateOrbs(OrbSlot[,] orbSlots) {
        Orb[] orbs = new Orb[this.width * this.height];
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                OrbSlot slot = orbSlots[j, i];
                Orb orb = CreateOrb(slot.position);
                orbs[i * this.width + j] = orb;
                slot.orb = orb;
            }
        }
        return orbs;
    }
    public OrbGoal[,] GenerateDefaultOrbGoals(Vector2 startingPosition) {
        OrbGoal[,] orbGoals = new OrbGoal[2, this.height];
        for (int i = 0; i < this.height; i++) {
            Vector3 orbSetting1Position = new Vector3(startingPosition.x - Orb.SIZE, startingPosition.y + i * Orb.SIZE, this.transform.position.z);
            orbGoals[0, i] = CreateOrbSetting(orbSetting1Position);

            Vector3 orbSetting2Position = new Vector3(startingPosition.x + (this.width) * Orb.SIZE, startingPosition.y + i * Orb.SIZE, this.transform.position.z);
            orbGoals[1, i] = CreateOrbSetting(orbSetting2Position);
        }
        return orbGoals;
    }
    private OrbSlot CreateOrbSlot(Vector3 worldPosition) {
        GameObject slotObject = (GameObject)Instantiate(this.OrbSlotPrefab, worldPosition, this.transform.rotation);
        slotObject.transform.parent = this.transform;
        OrbSlot orbSlot = slotObject.GetComponent<OrbSlot>();
        orbSlot.position = worldPosition;
        return orbSlot;
    }
    private Orb CreateOrb(Vector3 worldPosition) {
        GameObject orbObject = (GameObject)Instantiate(this.OrbPrefab, worldPosition, this.transform.rotation);
        orbObject.transform.parent = this.transform;
        return orbObject.GetComponent<Orb>();
    }
    private OrbGoal CreateOrbSetting(Vector3 worldPosition) {
        GameObject orbSettingObject = (GameObject)Instantiate(this.OrbSettingPrefab, worldPosition, this.transform.rotation);
        orbSettingObject.transform.parent = this.transform;
        return orbSettingObject.GetComponent<OrbGoal>();
    }
    public void SetDimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }
    public void SetDefaultColors(OrbGoal[,] orbSettings, Orb[] orbs) {
        for (int row = 0; row < this.height; row++) {
            for (int col = 0; col < this.width; col++) {
                orbs[row * width + col].setColor(row);
            }
            orbSettings[0, row].setColor(row);
            orbSettings[1, row].setColor(row);
        }
    }
}
