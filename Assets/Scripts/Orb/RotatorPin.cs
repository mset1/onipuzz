﻿using UnityEngine;
using System.Collections;

public class RotatorPin : MonoBehaviour {
    public OrbRotator rotator;
    public int xPosition = 0;
    public int yPosition = 0;
    public Collider2D pinCollider;

    private void Awake() {
        this.pinCollider = this.GetComponent<Collider2D>();
    }
    public void SetRotatorPosition() {
        this.rotator.currentX = this.xPosition;
        this.rotator.currentY = this.yPosition;
        this.rotator.transform.position = this.transform.position;
    }
    public void SetPosition(int x, int y) {
        this.xPosition = x;
        this.yPosition = y;
    }
}