﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour {
    public const float SIZE = 1f;
    public int type;
    private SpriteRenderer spriteRenderer;
    //public static Color[] ORB_COLORS = new Color[] {
    //    new Color(0.75f, 0f, 0f),
    //    new Color(0f, 0.75f, 0f),
    //    new Color(0f, 0f, 0.75f),
    //    new Color(0.9f, 0.9f, 0f),
    //    new Color(0.9f, 0f, 0.9f),
    //    new Color(0f, 0.9f, 0.9f)
    //};
    public static Color[] ORB_COLORS = new Color[] {
        new Color(1f, 0.3f, 0.3f),
        new Color(0.3f, 1f, 0.3f),
        new Color(0.3f, 0.3f, 1f),
        new Color(1f, 1f, 0.3f),
        new Color(1f, 0.3f, 1f),
        new Color(0.3f, 1f, 1f)
    };

    public void Awake() {
        this.spriteRenderer = this.GetComponent<SpriteRenderer>();
    }
    public void setColor(int colorIndex) {
        while (colorIndex >= Orb.ORB_COLORS.Length) {
            colorIndex -= Orb.ORB_COLORS.Length;
        }
        this.spriteRenderer.color = Orb.ORB_COLORS[colorIndex];
        this.type = colorIndex;
    }
}
