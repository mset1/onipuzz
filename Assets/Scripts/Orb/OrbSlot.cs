﻿using UnityEngine;
using System.Collections;

public class OrbSlot : MonoBehaviour {
    public Orb orb;
    public Vector2 position;
    public SpriteRenderer spriteRenderer;

    public static Color[] SLOT_COLORS = new Color[] {
        new Color(0.6f, 0.4f, 0.1f),
        new Color(0f, 1f, 1f)
    };
    public void Awake() {
        this.spriteRenderer = this.GetComponent<SpriteRenderer>();
        this.spriteRenderer.color = OrbSlot.SLOT_COLORS[0];
    }
    public void SetOrbPosition(Orb orb) {
        orb.transform.position = this.transform.position;
        orb.transform.rotation = this.transform.rotation;
    }
}
