﻿using UnityEngine;
using System.Collections;

public class OrbGoal : MonoBehaviour {
    public int type;
    public Vector2 position;
    private SpriteRenderer gemRenderer;
    private SpriteRenderer highlightRenderer;

    public static Color[] SLOT_COLORS = new Color[] {
        new Color(0.5f, 0.5f, 0.5f),
        new Color(0.7f, 1f, 1f)
    };

    public void Awake() {
        Transform orbTransform = this.transform.Find("Orb");
        this.gemRenderer = orbTransform.GetComponent<SpriteRenderer>();
        this.highlightRenderer = this.GetComponent<SpriteRenderer>();
    }
    public void setColor(int colorIndex) {
        while (colorIndex >= Orb.ORB_COLORS.Length) {
            colorIndex -= Orb.ORB_COLORS.Length;
        }
        this.gemRenderer.color = Orb.ORB_COLORS[colorIndex];
        this.type = colorIndex;
    }
    public void SetActive(bool isActive) {
        if (isActive) {
            this.highlightRenderer.color = OrbGoal.SLOT_COLORS[1];
        } else {
            this.highlightRenderer.color = OrbGoal.SLOT_COLORS[0];
        }
    }
}
