﻿using UnityEngine;
using System.Collections;

public class ScreenshotHandler : MonoBehaviour {
    private static ScreenshotHandler instance;
    private Camera myCamera;
    public static byte[] preparedScreenshot;
    private bool takeScreenshotOnNextFrame;

    private static int TEXTURE_SIZE = 180;

    private void Awake() {
        instance = this;
        this.myCamera = this.GetComponent<Camera>();
    }
    public static void PrepareScreenshotStatic() {
        instance.PrepareScreenshot();
    }
    private void OnPostRender() {
        if (!takeScreenshotOnNextFrame) { return; }
        takeScreenshotOnNextFrame = false;
        myCamera.targetTexture = RenderTexture.GetTemporary(TEXTURE_SIZE, TEXTURE_SIZE, 16);
        RenderTexture renderTexture = myCamera.targetTexture;

        float rectX = (Screen.width - Screen.height) / 2;

        Texture2D renderResult = new Texture2D(TEXTURE_SIZE, TEXTURE_SIZE, TextureFormat.ARGB32, false);
        Rect rect = new Rect(0, 0, TEXTURE_SIZE, TEXTURE_SIZE);
        renderResult.ReadPixels(rect, 0, 0, false);

        preparedScreenshot = renderResult.EncodeToPNG();
        Destroy(renderResult);
        myCamera.enabled = false;
    }
    private void PrepareScreenshot() {
        myCamera.enabled = true;
        takeScreenshotOnNextFrame = true;
        myCamera.targetTexture = RenderTexture.GetTemporary(TEXTURE_SIZE, TEXTURE_SIZE, 16);
    }
}
