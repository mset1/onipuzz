﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardGenerator : MonoBehaviour {
    private static GameObject OrbBoardPrefab;
    private static OniPuzzleGame oniPuzzleGame;
    private static int rotatorWidth = 1;
    private static int rotatorHeight = 1;

    public static OrbBoard GenerateBasicBoard(int width, int height) {
        GameObject boardObject = (GameObject)Instantiate(OrbBoardPrefab);
        OrbBoard board = boardObject.GetComponent<OrbBoard>();
        board.SetDimensions(width, height);
        board.Initialize();
        board.rotator.SetGame(oniPuzzleGame);
        board.GenerateBoardParameters();
        return board;
    }
    public static OrbBoard GenerateRandomBoard(int width, int height, int moves) {
        OrbBoard board = GenerateBasicBoard(width, height);
        RandomizeBoard(board, moves);
        board.GenerateBoardParameters();
        board.parameters.parMoves = moves;
        return board;
    }
    public static OrbBoard LoadBoard(BoardParameters parameters) {
        if (parameters == null) {
            Debug.Log("Trying to load null board parameters");
            return null; 
        }
        OrbBoard board = GenerateBasicBoard(parameters.width, parameters.height);
        board.LoadBoardParameters(parameters);
        return board;
    }
    public static void SetOrbBoardPrefab(GameObject prefab) {
        OrbBoardPrefab = prefab;
    }
    public static void SetGame(OniPuzzleGame game) {
        oniPuzzleGame = game;
    }
    private static void RandomizeBoard(OrbBoard orbBoard, int moves) {
        rotatorWidth = orbBoard.width - 1;
        rotatorHeight = orbBoard.height - 1;
        RandomRearrangeSmart(orbBoard, moves);
    }
    private static void RandomRearrange(OrbBoard orbBoard, int moves) {
        int[,] rotationCount = new int[rotatorWidth, rotatorHeight];
        if (moves > 2 * rotatorWidth * rotatorHeight) {
            moves = 2 * rotatorWidth * rotatorHeight;
        }
        for (int i = 0; i < moves; i++) {
            int col = Random.Range(0, rotatorWidth);
            int row = Random.Range(0, rotatorHeight);
            while (rotationCount[col, row] >= 2) {
                col = Random.Range(0, rotatorWidth);
                row = Random.Range(0, rotatorHeight);
            }
            orbBoard.RotateOrbsCCW(col, row);
        }
    }
    private static void RandomRearrangeSmart(OrbBoard board, int moves) {
        List<RotationEvent> unaffectedRotations = new List<RotationEvent>();
        List<long> boardHashes = new List<long>();
        for (int i = 0; i < moves; i++) {
            RotationEvent rotationEvent = GetValidRotation(unaffectedRotations);
            if (rotationEvent == null) {
                Debug.Log("Could not find additional moves at:" + i + " moves");
                return; 
            }
            unaffectedRotations = RemoveAdjacentEvents(rotationEvent, unaffectedRotations);
            if (IsTwoColorCrossRotation(rotationEvent, board)) {
                rotationEvent.SetMaxRepeats(1);
            }
            unaffectedRotations.Add(rotationEvent);
            board.RotateOrbsCCW(rotationEvent.xPos, rotationEvent.yPos);
        }
    }
    private static RotationEvent GetValidRotation(List<RotationEvent> unaffectedRotations) {
        int col = -1;
        int row = -1;
        int moveAttempts = 0;
        RotationEvent rotationEvent;
        do {
            col = Random.Range(0, rotatorWidth);
            row = Random.Range(0, rotatorHeight);
            rotationEvent = new RotationEvent(col, row, false);
            moveAttempts += 1;
        } while (
            !IsValidRotation(rotationEvent, unaffectedRotations)
            && moveAttempts < 20);
        if (moveAttempts >= 20) {
            return null;
        }
        return rotationEvent;
    }
    private static bool ContainsOldBoardHash(long newHash, List<long> boardHashes) {
        foreach (long value in boardHashes) {
            if (newHash == value) {
                return true;
            }
        }
        return false;
    }
    private static bool IsValidRotation(RotationEvent rotationEvent, List<RotationEvent> unaffectedRotations) {
        int instancesOfEvent = 0;
        foreach (RotationEvent re in unaffectedRotations) {
            if (re.IsReverse(rotationEvent)) { return false; }
            if (re.IsEquivalent(rotationEvent)) { instancesOfEvent += 1; }
            if (instancesOfEvent >= re.maxRepeats) { return false; }
        }
        return true;
    }
    private static bool IsTwoColorCrossRotation(RotationEvent rotationEvent, OrbBoard board) {
        OrbSlot[] slots = board.GetSlotsAtPosition(rotationEvent.xPos, rotationEvent.yPos);
        if (slots[0].orb.type == slots[2].orb.type
            && slots[1].orb.type == slots[3].orb.type) {
            return true;
        }
        return false;
    }
    private static List<RotationEvent> RemoveAdjacentEvents(RotationEvent rotationEvent, List<RotationEvent> unaffectedRotations) {
        List<RotationEvent> newUnaffected = new List<RotationEvent>();
        foreach(RotationEvent re in unaffectedRotations) {
            if (!rotationEvent.IsAdjacent(re)) {
                newUnaffected.Add(re);
            }
        }
        return newUnaffected;
    }
    private class RotationEvent {
        public int xPos = 0;
        public int yPos = 0;
        public bool isCWRotation = true;
        public int maxRepeats = 2;
        public int orbHash = 0;
        public RotationEvent(int xPos, int yPos, bool isCW) {
            this.xPos = xPos;
            this.yPos = yPos;
            this.isCWRotation = isCW;
        }
        private bool IsInSamePosition(RotationEvent other) {
            return (this.xPos == other.xPos 
                && this.yPos == other.yPos);
        }
        public bool IsAdjacent(RotationEvent other) {
            return (Mathf.Abs(this.xPos - other.xPos) <= 1
                && Mathf.Abs(this.yPos - other.yPos) <= 1
                && !IsInSamePosition(other));
        }
        public bool IsEquivalent(RotationEvent other) {
            return (this.IsInSamePosition(other) 
                && this.isCWRotation == other.isCWRotation);
        }
        public bool IsReverse(RotationEvent other) {
            return (this.IsInSamePosition(other) 
                && this.isCWRotation != other.isCWRotation);
        }
        public void SetMaxRepeats(int repeats) {
            this.maxRepeats = repeats;
        }
        public void SetOrbHash(OrbSlot[] slots) {
            int baseNumber = 10;
            for (int i = 0; i < slots.Length; i++) {
                this.orbHash += slots[i].orb.type * (int)Mathf.Pow(baseNumber, i);
            }
        }
        public override string ToString() {
            string direction = "cw";
            if (!isCWRotation) { direction = "ccw"; }
            return "{(" + xPos + "," + yPos + "), " + direction + "}";
        }
    }
}
