﻿using UnityEngine;
using System.Collections;
using System.IO;

public class OniPuzzleSandbox : OniPuzzleGame {
    private int width = 3;
    private int height = 4;
    private int parMoves = 8;
    private UnityEngine.UI.InputField parField;
    private UnityEngine.UI.InputField widthField;
    private UnityEngine.UI.InputField heightField;
    private UnityEngine.UI.InputField loadIdField;
    private UnityEngine.UI.InputField deleteIdField;

    private void Awake() {
        base.SetupUIElements();
        this.InitializeSandboxUIElements();
        LevelWriter.Load();
        this.InitializeBoardGenerator();
        base.InitializeEffects();
        Debug.Log("Setting up");
    }
    private void InitializeSandboxUIElements() {
        this.parField = GameObject.Find("ParField").GetComponent<UnityEngine.UI.InputField>();
        this.widthField = GameObject.Find("WidthField").GetComponent<UnityEngine.UI.InputField>();
        this.heightField = GameObject.Find("HeightField").GetComponent<UnityEngine.UI.InputField>();
        this.deleteIdField = GameObject.Find("DeleteIdField").GetComponent<UnityEngine.UI.InputField>();
        this.parField.text = "" + this.parMoves;
        this.widthField.text = "" + this.width;
        this.heightField.text = "" + this.height;
        this.loadIdField = GameObject.Find("LoadIdText").GetComponent<UnityEngine.UI.InputField>();
    }

    //private IEnumerator RetakeLevelThumbnailScreenshots() {
    //    yield return new WaitForSeconds(5f);
    //    Debug.Log("Initiating");
    //    for (int i = 0; i < LevelWriter.boardCollection.boards.Count; i++) {
    //        this.loadIdField.text = "" + i;
    //        LoadBoard();
    //        yield return new WaitForSeconds(0.2f);
    //        StoreBoard();
    //        yield return new WaitForSeconds(0.2f);
    //    }
    //}

    public void SaveBoard() {
        if (this.currentBoard == null) { return; }
        this.currentBoard.SaveBoardState();
        this.currentParameters = this.currentBoard.parameters.clone();
        ScreenshotHandler.PrepareScreenshotStatic();
    }
    // Developer Functions for Creating Levels
    public void GenerateRandomBoard() {
        if (this.currentBoard != null) { Destroy(this.currentBoard.gameObject); }
        this.currentBoard = BoardGenerator.GenerateRandomBoard(this.width, this.height, this.parMoves);
        this.currentBoard.parameters.id = LevelWriter.NUMBER_SAVES;
        this.currentParameters = this.currentBoard.parameters.clone();
        this.OnReset();
    }
    public void StoreBoard() {
        if (this.currentBoard == null) { return; }
        BoardParameters parameters = this.currentBoard.parameters;
        if (parameters.id < 0) { parameters.id = LevelWriter.NUMBER_SAVES; }

        if (this.currentMoves < parameters.parMoves && this.currentMoves > 0) {
            parameters.parMoves = this.currentMoves;
        }
        if (this.currentMoves > this.parMoves) {
            parameters.parMoves = this.parMoves;
        }
        LevelWriter.Save(parameters);
    }
    public void LoadBoard() {
        int loadId = int.Parse(this.loadIdField.text);
        LevelWriter.Load();
        if (this.currentBoard != null) { Destroy(this.currentBoard.gameObject); }
        this.currentBoard = BoardGenerator.LoadBoard(LevelWriter.GetBoardParameters(loadId));
        this.currentParameters = this.currentBoard.parameters.clone();
        this.parText.text = ""+this.currentParameters.parMoves;
        this.OnReset();
    }
    public void DeleteBoard() {
        int deleteId = int.Parse(this.deleteIdField.text);
        LevelWriter.Remove(deleteId);
    }
    // ----------------------------------------
    protected override void OnVictory() {
        Debug.Log("Victory Sandbox");
        this.victoryPanel.SetActive(true);
        RotatorController controller = this.currentBoard.rotator.GetComponent<RotatorController>();
        controller.isActive = false;
    }
    protected override void OnReset() {
        this.victoryPanel.SetActive(false);
        this.ResetMoves();
        this.SaveBoard();
        ScreenshotHandler.PrepareScreenshotStatic();
        this.CCWButton.onClick.AddListener(() => this.currentBoard.rotator.RotateCCWAtPosition());
        this.CWButton.onClick.AddListener(() => this.currentBoard.rotator.RotateCWAtPosition());
    }
    public void SetWidth() {
        this.width = int.Parse(this.widthField.text);
    }
    public void SetHeight() {
        this.height = int.Parse(this.heightField.text);
    }
    public void SetPar() {
        this.parMoves = int.Parse(this.parField.text);
    }
}
