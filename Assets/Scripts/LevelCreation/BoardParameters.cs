﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BoardParameters {
    public enum Packs {
        Pack3x3,
        Pack4x3,
        Pack3x4,
        Pack4x4,
        Pack3x5,
        Pack4x5,
        Pack4x6,
        Tutorial,
        None
    }

    public int id = -1;
    public int parMoves;
    public int width;
    public int height;
    public Packs packId = Packs.None;
    public int levelId = -1;
    public int[] orbColors;
    public int[] goalColors;

    public BoardParameters clone() {
        BoardParameters clone = new BoardParameters();
        clone.id = this.id;
        clone.parMoves = this.parMoves;
        clone.width = this.width;
        clone.height = this.height;
        clone.packId = this.packId;
        clone.levelId = this.levelId;
        clone.orbColors = new int[this.orbColors.Length];
        for (int i = 0; i < this.orbColors.Length; i++) {
            clone.orbColors[i] = this.orbColors[i];
        }
        clone.goalColors = new int[this.goalColors.Length];
        for (int i = 0; i < this.goalColors.Length; i++) {
            clone.goalColors[i] = this.goalColors[i];
        }
        return clone;
    }
}