﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardSolver : MonoBehaviour {
    private static int BASE_PAR_MOVES = 4;
    private static int BOARD_WIDTH = 2;
    private static int BOARD_HEIGHT = 1;

    private void Awake() {
        SolveBoard();
    }
    public static void SolveBoard() {
        float current = Time.realtimeSinceStartup;
        List<RotationEvent> possibleRotationEvents = GetPossibleRotationEvents(BOARD_WIDTH, BOARD_HEIGHT);
        List<List<RotationEvent>> moveCombos = IteratePossibleCombinations(new List<RotationEvent>(), possibleRotationEvents);
        for (int moveCount = 1; moveCount < BASE_PAR_MOVES; moveCount++) {
            Debug.Log("moveCount:" + moveCount);
            List<List<RotationEvent>> sequencesToAdd = new List<List<RotationEvent>>();

            foreach (List<RotationEvent> existingMoves in moveCombos) {
                if (existingMoves.Count != moveCount) {
                    continue; 
                }
                List<List<RotationEvent>> newMoveCombos = IteratePossibleCombinations(existingMoves, possibleRotationEvents);
                foreach (List<RotationEvent> sequence in newMoveCombos) {
                    sequencesToAdd.Add(sequence);
                }
            }
            sequencesToAdd = RemoveUndosAndTriples(sequencesToAdd);
            sequencesToAdd = RemoveMirrors(sequencesToAdd);
            foreach (List<RotationEvent> sequence in sequencesToAdd) {
                moveCombos.Add(sequence);
            }
        }
        string s = "";
        foreach(List<RotationEvent> sequence in moveCombos) {
            s += DebugPrintListMoves(sequence) + "\n";
        }
        Debug.Log(s);
        Debug.Log("Sequence Combinations: " + moveCombos.Count + "; " + (Time.realtimeSinceStartup - current));
    }
    private static string DebugPrintListMoves(List<RotationEvent> moves) {
        string s = "";
        foreach (RotationEvent move in moves) {
            s += move.ToString();
        }
        return s;
    }
    private static List<List<RotationEvent>> IteratePossibleCombinations(List<RotationEvent> existingMoves, List<RotationEvent> possibleMoves) {
        List<List<RotationEvent>> newMoveCombos = new List<List<RotationEvent>>();
        for (int xPos = 0; xPos < BOARD_WIDTH; xPos++) {
            for (int yPos = 0; yPos < BOARD_HEIGHT; yPos++) {
                for (int dir = 0; dir < 2; dir++) {
                    List<RotationEvent> moveCombo = new List<RotationEvent>(existingMoves);
                    RotationEvent potentialMove = new RotationEvent(xPos, yPos, dir == 1);
                    moveCombo.Add(potentialMove);
                    newMoveCombos.Add(moveCombo);
                }
            }
        }
        return newMoveCombos;
    }
    private static List<List<RotationEvent>> RemoveReversedDoubles(List<List<RotationEvent>> sequences) {
        List<List<RotationEvent>> editedSequences = new List<List<RotationEvent>>();
        List<RotationEvent> pinsWithDoubles = new List<RotationEvent>();
        foreach (List<RotationEvent> sequence in sequences) {
            bool isReversedDouble = false;
            if (IsEndedWithDouble(sequence)) {
                RotationEvent lastEvent = sequence[sequence.Count - 1];
                foreach (RotationEvent pin in pinsWithDoubles) {
                    if (pin.isInSamePosition(lastEvent)) {
                        isReversedDouble = true;
                    }
                }
                if (!isReversedDouble) {
                    pinsWithDoubles.Add(lastEvent);
                }
            }
            if (!isReversedDouble) {
                editedSequences.Add(sequence);
            }
        }
        return editedSequences;
    }
    private static bool IsEndedWithDouble(List<RotationEvent> sequence) {
        int sequenceLength = sequence.Count;
        if (sequenceLength >= 2) {
            if (sequence[sequenceLength - 1].isEquivalent(sequence[sequenceLength - 2])) {
                return true;
            }
        }
        return false;
    }
    private static List<RotationEvent> GetPossibleRotationEvents(int width, int height) {
        List<RotationEvent> possibleEvents = new List<RotationEvent>();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int dir = 0; dir < 2; dir++) {
                    RotationEvent re = new RotationEvent(x, y, dir == 1);
                    possibleEvents.Add(re);
                }
            }
        }
        return possibleEvents;
    }
    private static List<List<RotationEvent>> RemoveMirrors(List<List<RotationEvent>> sequences) {
        List<List<RotationEvent>> editedSequences = new List<List<RotationEvent>>();
        List<RotationCounterGrid> grids = new List<RotationCounterGrid>();
        foreach (List<RotationEvent> sequence in sequences) {
            bool hasMirror = false;
            RotationCounterGrid tempGrid = new RotationCounterGrid(BOARD_WIDTH, BOARD_HEIGHT);
            foreach (RotationEvent rotationEvent in sequence) {
                tempGrid.AddRotationEvent(rotationEvent);
            }
            foreach (RotationCounterGrid grid in grids) {
                if (tempGrid.HasEquivalentMirrors(grid)) {
                    hasMirror = true;
                }
            }
            if (!hasMirror) {
                editedSequences.Add(sequence);
                grids.Add(tempGrid);
            }
        }

        return editedSequences;
    }
    private static List<List<RotationEvent>> RemoveUndosAndTriples(List<List<RotationEvent>> sequences) {
        List<List<RotationEvent>> editedSequences = new List<List<RotationEvent>>();
        foreach (List<RotationEvent> sequence in sequences) {
            RotationCounterGrid grid = new RotationCounterGrid(BOARD_WIDTH, BOARD_HEIGHT);
            bool hasUndoOrTriple = false;
            foreach (RotationEvent rotationEvent in sequence) {
                if (!grid.AddRotationEvent(rotationEvent)) {
                    hasUndoOrTriple = true;
                }
            }
            if (!hasUndoOrTriple) {
                editedSequences.Add(sequence);
            }
        }

        return editedSequences;
    }
    public class RotationCounterGrid {
        public RotationCounter[,] grid;
        public int width;
        public int height;
        public RotationCounterGrid(int width, int height) {
            grid = new RotationCounter[width, height];
            for (int row = 0; row < height; row++) {
                for (int col = 0; col < width; col++) {
                    grid[col, row] = new RotationCounter(col, row);
                }
            }
            this.width = width;
            this.height = height;
        }
        public bool HasEquivalentMirrors(RotationCounterGrid other) {
            if (!(other.width == this.width && other.height == this.height)) {
                return false;
            }
            for (int row = 0; row < height; row++) {
                for (int col = 0; col < width; col++) {
                    if (Mathf.Abs(this.grid[col, row].rotationCount) == 2 &&
                        Mathf.Abs(other.grid[col, row].rotationCount) == 2) {
                        return true;
                    }
                }
            }
            return false;
        }
        private void ResetAdjacentCounters(RotationEvent rotation) {
            if (rotation.xPos-1 >= 0) { grid[rotation.xPos - 1, rotation.yPos].ResetRotationCounter(); }
            if (rotation.xPos+1 < width) { grid[rotation.xPos + 1, rotation.yPos].ResetRotationCounter(); }
            if (rotation.yPos-1 >= 0) { grid[rotation.xPos, rotation.yPos-1].ResetRotationCounter(); }
            if (rotation.yPos+1 < height) { grid[rotation.xPos, rotation.yPos + 1].ResetRotationCounter(); }
        }
        public bool AddRotationEvent(RotationEvent rotation) {
            if (!Has180Rotation(rotation) && !HasOpposingRotation(rotation)) {
                grid[rotation.xPos, rotation.yPos].AddRotationEvent(rotation);
                ResetAdjacentCounters(rotation);
                return true;
            }
            return false;
        }
        private bool Has180Rotation(RotationEvent rotation) {
            return Mathf.Abs(grid[rotation.xPos, rotation.yPos].rotationCount) == 2;
        }
        private bool HasOpposingRotation(RotationEvent rotation) {
            return grid[rotation.xPos, rotation.yPos].IsOpposingRotation(rotation);
        }
    }
    public class RotationCounter {
        public int xPos = 0;
        public int yPos = 0;
        public int rotationCount = 0;
        public RotationCounter(int x, int y) {
            this.xPos = x;
            this.yPos = y;
        }
        public int ResetRotationCounter() {
            this.rotationCount = 0;
            return this.rotationCount;
        }
        public int AddRotationEvent(RotationEvent rotationEvent) {
            if (rotationEvent.isCWRotation) { rotationCount += 1; } 
            else { rotationCount += -1; }
            return this.rotationCount;
        }
        public bool IsOpposingRotation(RotationEvent rotationEvent) {
            if (isInSamePosition(rotationEvent)) {
                int direction = 1;
                if (!rotationEvent.isCWRotation) { direction = -1; }
                return direction * this.rotationCount < 0;
            }
            return false;
        }
        public bool isInSamePosition(RotationEvent other) {
            return (this.xPos == other.xPos && this.yPos == other.yPos);
        }
        public static RotationCounter[,] CreateRotationCounterGrid(int width, int height) {
            RotationCounter[,] rotationCounters = new RotationCounter[width, height];
            for (int row = 0; row < height; row++) {
                for (int col = 0; col < width; col++) {
                    rotationCounters[col, row] = new RotationCounter(col, row);
                }
            }
            return rotationCounters;
        }
        public override string ToString() {
            return "" + rotationCount;
        }
    }
    public class RotationEvent {
        public int xPos = 0;
        public int yPos = 0;
        public bool isCWRotation = true;
        public RotationEvent(int xPos, int yPos, bool isCW) {
            this.xPos = xPos;
            this.yPos = yPos;
            this.isCWRotation = isCW;
        }
        public bool isInSamePosition(RotationEvent other) {
            return (this.xPos == other.xPos && this.yPos == other.yPos);
        }
        public bool isEquivalent(RotationEvent other) {
            return (this.isInSamePosition(other) && this.isCWRotation == other.isCWRotation);
        }
        public bool isOpposite(RotationEvent other) {
            return (this.isInSamePosition(other) && this.isCWRotation != other.isCWRotation);
        }
        public override string ToString() {
            string direction = "0";
            if (isCWRotation) { direction = "1"; }
            return "" + xPos + "" + direction + "\t";
        }
    }
}
