﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class RotatorController : MonoBehaviour {
    private OrbRotator rotator;
    public bool isActive = true;
    private EventSystem eventSystem;

    private KeyCode move_left { get; set; }
    private KeyCode move_right { get; set; }
    private KeyCode move_up { get; set; }
    private KeyCode move_down { get; set; }
    private KeyCode select { get; set; }
    private KeyCode cancel { get; set; }

    private int framesSinceLastInput = 0;
    private static int FRAMES_BEFORE_HINT = 400;
    private static float TIME_BETWEEN_FRAMES = 0.02f;

    private void Awake() {
        eventSystem = FindObjectOfType<EventSystem>();
        this.rotator = this.GetComponent<OrbRotator>();
        this.SetupKeys();
        StartCoroutine(TrackTimeSinceInput());
    }
    private void Update() {
        if (!this.isActive) { return; }
        this.DetectKeyboardInput();
        this.DetectMouseInput();
    }
    private void SetupKeys() {
        string move_left_str = this.gameObject.name + "move_left_key";
        string move_right_str = this.gameObject.name + "move_right_key";
        string move_up_str = this.gameObject.name + "move_up_key";
        string move_down_str = this.gameObject.name + "move_down_key";
        string select_str = this.gameObject.name + "select_key";
        string cancel_str = this.gameObject.name + "cancel_key";


        move_left = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(move_left_str, KeyCode.LeftArrow.ToString()));
        move_right = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(move_right_str, KeyCode.RightArrow.ToString()));
        move_up = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(move_up_str, KeyCode.UpArrow.ToString()));
        move_down = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(move_down_str, KeyCode.DownArrow.ToString()));
        select = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(select_str, KeyCode.Z.ToString()));
        cancel = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(cancel_str, KeyCode.X.ToString()));
    }
    public virtual void DetectKeyboardInput() {
        if (Input.GetKeyUp(this.move_left)) {
            this.rotator.MoveRotator(-1, 0);
        }
        if (Input.GetKeyUp(this.move_right)) {
            this.rotator.MoveRotator(1, 0);
        }
        if (Input.GetKeyUp(this.move_up)) {
            this.rotator.MoveRotator(0, 1);
        }
        if (Input.GetKeyUp(this.move_down)) {
            this.rotator.MoveRotator(0, -1);
        }
        if (Input.GetKeyUp(this.select)) {
            this.Rotate(false);
        }
        if (Input.GetKeyUp(this.cancel)) {
            this.Rotate(true);
        }
    }
    public virtual void DetectMouseInput() {
        if (eventSystem != null && eventSystem.IsPointerOverGameObject()) {
            return;
        }
        if (Input.GetMouseButtonUp(0)) {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null) {
                this.rotator.MoveRotatorToPin(hit.collider);
                this.framesSinceLastInput = 0;
                AudioManager.PlayInsertSound();
            }
        }
    }
    private IEnumerator TrackTimeSinceInput() {
        while (true) {
            if (this.isActive) {
                this.framesSinceLastInput += 1;
            } else {
                this.framesSinceLastInput = 0;
            }
            if (this.framesSinceLastInput >= FRAMES_BEFORE_HINT) {
                EffectsHandler.startFlashingNails = true;
            } else {
                EffectsHandler.startFlashingNails = false;
            }
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
    }
    public void Rotate(bool isCW) {
        if (!this.isActive) { return; }
        if (isCW) {
            this.rotator.RotateCWAtPosition();
        } else {
            this.rotator.RotateCCWAtPosition();
        }
    }
}
