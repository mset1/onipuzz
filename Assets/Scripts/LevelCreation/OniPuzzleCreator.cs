﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class OniPuzzleCreator : OniPuzzleGame {
    private KeyCode randomize_key { get; set; }

    private int width = 4;
    private int height = 6;
    private int parMoves = 10;
    private GameObject randomizePanel;
    private UnityEngine.UI.InputField parField;
    private UnityEngine.UI.InputField widthField;
    private UnityEngine.UI.InputField heightField;

    private GameObject memoryPanel;
    private UnityEngine.UI.InputField loadField;
    private UnityEngine.UI.InputField deleteField;

    private UnityEngine.UI.Dropdown packDropdown;
    private UnityEngine.UI.InputField levelField;

    private UnityEngine.UI.Text idField;

    private void Awake() {
        base.SetupUIElements();
        base.InitializeBoardGenerator();
        this.InitializeCreationUI();
        this.GenerateRandomBoard();
        this.SetupKeys();

        //StartCoroutine(RetakePictures());
    }
    private IEnumerator RetakePictures() {
        Debug.Log("Number of boards: " + LevelWriter.NUMBER_SAVES);
        List<int> boardIds = new List<int>();
        foreach (BoardParameters board in LevelWriter.boardCollection.boards) {
            boardIds.Add(board.id);
        }
        for (int i = 0; i < boardIds.Count; i++) {
            this.loadField.text = "" + boardIds[i];
            LoadBoard();
            yield return new WaitForSeconds(0.1f);
            StoreBoard();
            yield return new WaitForSeconds(0.1f);
        }
    }
    protected override void SetupKeys() {
        base.SetupKeys();
        string randomizeReset_str = this.gameObject.name + "randomize_key";
        randomize_key = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(randomizeReset_str, KeyCode.R.ToString()));
    }
    private void InitializeCreationUI() {
        this.randomizePanel = GameObject.Find("RandomizePanel");
        this.parField = GameObject.Find("ShufflesInput").GetComponent<UnityEngine.UI.InputField>();
        this.widthField = GameObject.Find("WidthInput").GetComponent<UnityEngine.UI.InputField>();
        this.heightField = GameObject.Find("HeightInput").GetComponent<UnityEngine.UI.InputField>();
        this.parField.text = "" + this.parMoves;
        this.widthField.text = "" + this.width;
        this.heightField.text = "" + this.height;

        this.memoryPanel = GameObject.Find("MemoryPanel");
        this.loadField = GameObject.Find("LoadInput").GetComponent<UnityEngine.UI.InputField>();
        this.deleteField = GameObject.Find("DeleteInput").GetComponent<UnityEngine.UI.InputField>();

        this.idField = GameObject.Find("IDText").GetComponent<UnityEngine.UI.Text>();
        this.packDropdown = GameObject.Find("PackDropDown").GetComponent<UnityEngine.UI.Dropdown>();
        this.levelField = GameObject.Find("LevelInput").GetComponent<UnityEngine.UI.InputField>();

        foreach (string pack in System.Enum.GetNames(typeof(BoardParameters.Packs))) {
            this.packDropdown.options.Add(new UnityEngine.UI.Dropdown.OptionData() { text = pack });
        }
    }
    private void FixedUpdate() {
        if (this.currentBoard == null) { return; }
        this.idField.text = "" + this.currentBoard.parameters.id;
    }
    protected override void Update() {
        base.Update();
        if (Input.GetKeyUp(randomize_key)) {
            this.width = int.Parse(this.widthField.text);
            this.height = int.Parse(this.heightField.text);
            this.parMoves = int.Parse(this.parField.text);
            this.GenerateRandomBoard();
        }
    }
    private void GenerateRandomBoard() {
        if (this.currentBoard != null) { Destroy(this.currentBoard.gameObject); }
        this.currentBoard = BoardGenerator.GenerateRandomBoard(this.width, this.height, this.parMoves);
        this.currentBoard.parameters.id = GenerateBoardID();
        this.currentParameters = this.currentBoard.parameters.clone();
        this.parText.text = "" + this.currentParameters.parMoves;
        this.InitializeEffects();
        this.currentBoard.CheckBoardMatch();
        this.OnReset();
    }
    protected override void OnVictory() {
        RotatorController controller = this.currentBoard.rotator.GetComponent<RotatorController>();
        controller.isActive = false;
        EffectsHandler.HaloStatus haloStatus = EffectsHandler.HaloStatus.Solved;
        if (currentMoves <= this.currentParameters.parMoves) {
            haloStatus = EffectsHandler.HaloStatus.Par;
        }
        EffectsHandler.PlayVictoryCreationAnimation(haloStatus);
    }
    private int GenerateBoardID() {
        int biggestId = LevelWriter.NUMBER_SAVES;
        foreach (BoardParameters board in LevelWriter.boardCollection.boards) {
            if (board.id >= biggestId) {
                biggestId = board.id + 1;
            }
        }
        return biggestId;
    }
    public void OpenRandomize() {
        PanelManager.FadePartialDarken(true);
        PanelManager.FadePanel(this.randomizePanel, true);
        RotatorController controller = this.currentBoard.rotator.GetComponent<RotatorController>();
        controller.isActive = false;
    }
    public void CloseRandomize() {
        PanelManager.FadePartialDarken(false);
        PanelManager.FadePanel(this.randomizePanel, false);
        RotatorController controller = this.currentBoard.rotator.GetComponent<RotatorController>();
        controller.isActive = true;
    }
    public void RandomizeBoard() {
        this.width = int.Parse(this.widthField.text);
        this.height = int.Parse(this.heightField.text);
        this.parMoves = int.Parse(this.parField.text);
        this.CloseRandomize();
        this.GenerateRandomBoard();
    }
    public void CaptureBoard() {
        if (this.currentBoard == null) { return; }
        this.currentBoard.SaveBoardState();
        this.currentParameters = this.currentBoard.parameters.clone();
        this.OnReset();
    }
    public void OpenMemory() {
        PanelManager.FadePartialDarken(true);
        PanelManager.FadePanel(this.memoryPanel, true);
        RotatorController controller = this.currentBoard.rotator.GetComponent<RotatorController>();
        controller.isActive = false;
    }
    public void CloseMemory() {
        PanelManager.FadePartialDarken(false);
        PanelManager.FadePanel(this.memoryPanel, false);
        RotatorController controller = this.currentBoard.rotator.GetComponent<RotatorController>();
        controller.isActive = true;
    }
    public void LoadBoard() {
        int loadId = -1;
        if (this.loadField.text != "") {
            loadId = int.Parse(this.loadField.text);
        } else {
            string dropDownValue = this.packDropdown.options[this.packDropdown.value].text;
            BoardParameters.Packs packId = (BoardParameters.Packs)System.Enum.Parse(typeof(BoardParameters.Packs), dropDownValue);
            int levelId = int.Parse(this.levelField.text);
            loadId = LevelWriter.GetBoardId(packId, levelId);
        }
        this.loadField.text = "";
        if (this.currentBoard != null) { Destroy(this.currentBoard.gameObject); }
        this.currentBoard = BoardGenerator.LoadBoard(LevelWriter.GetBoardParameters(loadId));
        if (this.currentBoard == null) {
            Debug.Log(loadId + " failed to load");
        }
        this.currentParameters = this.currentBoard.parameters.clone();
        this.parText.text = "" + this.currentParameters.parMoves;
        this.InitializeEffects();
        this.currentBoard.CheckBoardMatch();
        
        for (int i = 0; i < this.packDropdown.options.Count; i++) {
            UnityEngine.UI.Dropdown.OptionData optionData = this.packDropdown.options[i];
            if (optionData.text == currentBoard.parameters.packId.ToString()) {
                this.packDropdown.value = i;
                break;
            }
        }
        this.levelField.text = "" + currentBoard.parameters.levelId;

        this.OnReset();
        this.CloseMemory();
    }
    public void DeleteBoard() {
        int deleteId = int.Parse(this.deleteField.text);
        Debug.Log("Deleting Board:" + deleteId);
        LevelWriter.Remove(deleteId);
    }
    public void StoreBoard() {
        if (this.currentBoard == null) { return; }
        BoardParameters parameters = this.currentBoard.parameters.clone();
        if (parameters.id < 0) { parameters.id = GenerateBoardID(); }
        if (this.currentMoves < parameters.parMoves && this.currentMoves > 0) {
            parameters.parMoves = this.currentMoves;
        }
        if (this.currentMoves > this.parMoves) {
            parameters.parMoves = this.parMoves;
        }
        string dropDownValue = this.packDropdown.options[this.packDropdown.value].text;
        parameters.packId = (BoardParameters.Packs)System.Enum.Parse(typeof(BoardParameters.Packs), dropDownValue);

        if (this.levelField.text == "") {
            parameters.levelId = -1;
        } else {
            parameters.levelId = int.Parse(this.levelField.text);
        }
        
        LevelWriter.Save(parameters);
    }
    protected override void OnReset() {
        base.OnReset();
        ScreenshotHandler.PrepareScreenshotStatic();
    }
}
