﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.Networking;

public static class LevelWriter {
    public static List<Sprite> boardSprites = new List<Sprite>();
    public static int NUMBER_SAVES = 0;
    public static string SAVE_DIRECTORY = Application.streamingAssetsPath;
    public static string IMAGE_SUBDIR = "/Images";
    public static string JSON_SAVE_NAME = "/levels.json";
    public static string BIT_SAVE_NAME = "/levels.gd";

    public static string TEMP_SAVE_DIR = Application.dataPath + "/Saves";

    [SerializeField]
    public static BoardParametersCollection boardCollection = new BoardParametersCollection();
    [System.Serializable]
    public class BoardParametersCollection {
        public List<BoardParameters> boards = new List<BoardParameters>();
    }

    public static void Remove(int id) {
        BoardParameters toRemove = LevelWriter.GetBoardParameters(id);
        if (toRemove == null) { 
            Debug.Log("Board does not exist with id:" + id);
            return;
        }
        Debug.Log("Removing board with id:" + id);
        RemoveFromPack(toRemove);
        boardCollection.boards.Remove(toRemove);
        File.Delete(SAVE_DIRECTORY + IMAGE_SUBDIR + GetImageName(toRemove));
        WriteBoardsToJSON();
        LevelWriter.NUMBER_SAVES = boardCollection.boards.Count;
    }
    public static void Save(BoardParameters parameters) {
        Save(parameters, true);
    }
    public static void Save(BoardParameters parameters, bool takePicture) {
        if (!ReplaceOldEntry(parameters)) {
            if (ContainsStartingPattern(parameters)) {
                Debug.Log("Will not add duplicate board");
                return;
            }
            boardCollection.boards.Add(parameters);
            Debug.Log("Saving board with id:" + parameters.id + ", imageName:" + GetImageName(parameters));
        } else {
            Debug.Log("Overwriting board: " + parameters.id);
        }
        InsertToPackLevel(parameters);
        if (takePicture) { WriteBoardToImage(parameters); }
        WriteBoardsToJSON();
        LevelWriter.NUMBER_SAVES = boardCollection.boards.Count;
    }
    public static void Load() {
        ReadBoardsFromJSON();
        LevelWriter.NUMBER_SAVES = boardCollection.boards.Count;
        Debug.Log("Level Writer: " + LevelWriter.NUMBER_SAVES);
        LoadSprites();
    }

    public static BoardParameters GetBoardParameters(int id) {
        foreach (BoardParameters parameters in boardCollection.boards) {
            if (id == parameters.id) {
                return parameters;
            }
        }
        return null;
    }
    public static BoardParameters GetBoardParameters(BoardParameters.Packs pack, int level) {
        foreach (BoardParameters board in boardCollection.boards) {
            if (board.packId == pack && board.levelId == level) {
                return board;
            }
        }
        return null;
    }
    public static int GetBoardId(BoardParameters.Packs pack, int level) {
        foreach (BoardParameters parameters in boardCollection.boards) {
            if (parameters.packId == pack && parameters.levelId == level) {
                return parameters.id;
            }
        }
        return -1;
    }
    public static Sprite GetSprite(int id) {
        for (int i = 0; i < boardCollection.boards.Count; i++) {
            if (boardCollection.boards[i].id == id) {
                return boardSprites[i];
            }
        }
        return null;
    }
    public static bool IsLoaded() {
        if (boardCollection.boards != null) {
            MainMenuHandler.statusText.text = "Boards found: " + boardCollection.boards.Count;
        }
        return !(boardCollection.boards == null || boardCollection.boards.Count == 0);
    }
    private static bool ReplaceOldEntry(BoardParameters parameters) {
        int oldIndex = -1;
        for (int i = 0; i < boardCollection.boards.Count; i++) {
            if (boardCollection.boards[i].id == parameters.id) {
                oldIndex = i;
            }
        }
        if (oldIndex >= 0) {
            if (parameters.packId != boardCollection.boards[oldIndex].packId) {
                RemoveFromPack(boardCollection.boards[oldIndex]);
            }
            boardCollection.boards[oldIndex] = parameters;
            return true;
        }
        return false;
    }
    private static bool ContainsStartingPattern(BoardParameters parameters) {
        Debug.Log("Looking for starting patterns");
        foreach (BoardParameters oldParameters in boardCollection.boards) {
            if (!(oldParameters.width == parameters.width &&
                oldParameters.height == parameters.height)) {
                continue;
            }
            bool intermediaryCheck = true;
            for (int i = 0; i < oldParameters.orbColors.Length; i++) {
                if (oldParameters.orbColors[i] != parameters.orbColors[i]) {
                    intermediaryCheck = false;
                }
            }
            if (intermediaryCheck) { return true; }
        }
        return false;
    }
    private static void InsertToPackLevel(BoardParameters parameters) {
        int oldLevel = -1;
        foreach (BoardParameters existingParameters in boardCollection.boards) {
            if (existingParameters.id == parameters.id) {
                oldLevel = existingParameters.levelId;
                Debug.Log("Old Level:" + oldLevel);
            }
        }
        if (parameters.levelId < 0) {
            parameters.levelId = 99999;
            Debug.Log("Inserting Last");
        }
        int largestLevel = -1;
        foreach (BoardParameters existingParameters in boardCollection.boards) {
            if (existingParameters.id == parameters.id) { continue; }
            if (existingParameters.packId != parameters.packId) { continue; }
            if (oldLevel > 0) {
                if (existingParameters.levelId >= parameters.levelId &&
                    existingParameters.levelId <= oldLevel) {
                    existingParameters.levelId += 1;
                }
            }
            if (existingParameters.levelId > largestLevel) {
                largestLevel = existingParameters.levelId;
            }
        }
        if (largestLevel < parameters.levelId) {
            parameters.levelId = largestLevel + 1;
        }
        Debug.Log("Inserting level:" + parameters.levelId + "into " + parameters.packId);
    }
    private static void RemoveFromPack(BoardParameters toRemove) {
        foreach (BoardParameters board in boardCollection.boards) {
            if (board.packId == toRemove.packId) {
                if (board.levelId > toRemove.levelId) {
                    board.levelId -= 1;
                }
            }
        }
        toRemove.packId = BoardParameters.Packs.None;
        toRemove.levelId = -1;
    }
    private static void SortByPar() {
        Debug.Log("Sorting boards");
        List<BoardParameters> orderedList = new List<BoardParameters>();
        int parCount = 0;
        while (orderedList.Count < boardCollection.boards.Count
            && parCount < 24) {
            foreach (BoardParameters parameters in boardCollection.boards) {
                if (parameters.parMoves == parCount) {
                    orderedList.Add(parameters);
                }
            }
            parCount += 1;
        }
        for (int i = 0; i < boardCollection.boards.Count; i++) {
            boardCollection.boards[i] = orderedList[i];
        }
    }

    public static void ReadBoardsFromJSON() {
        string filePath = SAVE_DIRECTORY + JSON_SAVE_NAME;
        string dataAsJson = "";

        if (Application.platform == RuntimePlatform.Android) {
            UnityWebRequest loadingRequest = LoadStreamingAssetFromAndroid(filePath);
            if (loadingRequest != null) {
                dataAsJson = loadingRequest.downloadHandler.text;
            }
        } else {
            dataAsJson = File.ReadAllText(SAVE_DIRECTORY + JSON_SAVE_NAME);
            if (MainMenuHandler.statusText != null) {
                MainMenuHandler.statusText.text = "Loading for PC...";
            }
        }
        
        boardCollection = JsonUtility.FromJson<BoardParametersCollection>(dataAsJson);
        LevelWriter.NUMBER_SAVES = boardCollection.boards.Count;
        Debug.Log("Loaded " + boardCollection.boards.Count + " boards.");
    }
    private static void LoadSprites() {
        for (int i = 0; i < boardCollection.boards.Count; i++) {
            string filename = SAVE_DIRECTORY + IMAGE_SUBDIR + GetImageName(boardCollection.boards[i]);
            if (!System.IO.File.Exists(filename)) {
                continue;
            }
            byte[] pngBytes = null;
            if (Application.platform == RuntimePlatform.Android) {
                UnityWebRequest loadingRequest = LoadStreamingAssetFromAndroid(filename);
                if (loadingRequest != null) {
                    pngBytes = loadingRequest.downloadHandler.data;
                }
                GameLoader.IMAGES_LOADED += 1;
            } else {
                pngBytes = System.IO.File.ReadAllBytes(filename);
                GameLoader.IMAGES_LOADED += 1;
            }
            if (pngBytes != null) {
                Texture2D testTexture = new Texture2D(120, 120);
                testTexture.LoadImage(pngBytes);
                testTexture.Apply();
                Sprite sprite = Sprite.Create(testTexture, new Rect(0, 0, testTexture.width, testTexture.height), new Vector2(0.5f, 0.5f));
                boardSprites.Add(sprite);
            }
        }
    }
    public static UnityWebRequest LoadStreamingAssetFromAndroid(string filePath) {
        if (Application.platform == RuntimePlatform.Android) {
            Debug.Log("Loading (Android): " + filePath);
            UnityWebRequest loadingRequest = UnityWebRequest.Get(filePath);
            loadingRequest.SendWebRequest();
            while (!loadingRequest.isDone) {
                if (loadingRequest.isNetworkError || loadingRequest.isHttpError) {
                    Debug.LogError("Web Request Error for file: " + filePath);
                    return null;
                }
            }
            Debug.Log("Successfully loaded file:" + filePath);
            return loadingRequest;
        }
        Debug.LogError("Attempting to load using Android for a non Android device");
        return null;
    }

    private static void WriteBoardsToJSON() {
        Debug.Log("Writing board to json");
        string levelsJson = JsonUtility.ToJson(boardCollection);
        File.WriteAllText(SAVE_DIRECTORY + JSON_SAVE_NAME, levelsJson);
    }
    private static void WriteBoardToImage(BoardParameters parameters) {
        string filename = SAVE_DIRECTORY + IMAGE_SUBDIR + GetImageName(parameters);
        Debug.Log("Writing board to image:" + filename);
        File.WriteAllBytes(filename, ScreenshotHandler.preparedScreenshot);
    }
    public static string GetImageName(BoardParameters parameters) {
        return "/board" + parameters.id + ".png";
    }
    public static string GetImageName(int index) {
        return "/board" + boardCollection.boards[index].id + ".png";
    }
}
