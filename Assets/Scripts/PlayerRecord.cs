﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerRecord {
    public int boardId = -1;
    public int bestMoves = -1;
}
