﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public static class CrownManager {
    public static Color SILVER_CROWN = new Color(0.625f, 0.75f, 0.75f);
    public static Color GOLD_CROWN = new Color(0.9f, 0.75f, 0.4f);

    public static void SetCrownFromAll(Image image) {
        SetCrownFromList(image, LevelWriter.boardCollection.boards);
    }
    public static bool SetCrownFromList(Image image, List<BoardParameters> parametersList) {
        bool isComplete = true;
        bool isGold = true;
        if (parametersList.Count <= 0) { isComplete = false; }
        foreach (BoardParameters parameters in parametersList) {
            PlayerRecord record = PlayerRecordKeeper.GetPlayerRecord(parameters.id);
            if (record == null) {
                isComplete = false;
            } else {
                if (record.bestMoves > parameters.parMoves) {
                    isGold = false;
                }
            }
        }
        if (isGold) {
            image.color = GOLD_CROWN;
        } else {
            image.color = SILVER_CROWN;
        }
        image.enabled = isComplete;
        return isComplete;
    }
    public static bool SetCrownImage(Image image, BoardParameters board) {
        PlayerRecord record = PlayerRecordKeeper.GetPlayerRecord(board.id);
        if (record == null) {
            image.enabled = false;
            return false; 
        }

        if (record.bestMoves <= board.parMoves) {
            image.color = GOLD_CROWN;
        } else {
            image.color = SILVER_CROWN;
        }
        image.enabled = true;
        return true;
    }
}
