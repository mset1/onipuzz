﻿using UnityEngine;
using System.Collections;

public class EffectsHandler : MonoBehaviour {
    public bool showHalos = true;

    public enum HaloStatus {
        Solved, Par, Loss
    }
    public static GameObject HaloPrefab;
    public static GameObject NailHaloPrefab;
    private static EffectsHandler instance;
    private static Camera mainCamera;
    private static GameObject victoryPanel;
    private static GameObject defeatPanel;
    private static SpriteRenderer[,] haloArray;
    private static SpriteRenderer[] nailHaloArray;

    private static Color NORMAL_HALO_COLOR = new Color(0.7f, 1f, 1f);
    private static Color SOLVED_HALO_COLOR = new Color(1f, 0.7f, 1f);
    private static Color PAR_HALO_COLOR = new Color(1f, 0.75f, 0.2f);
    private static Color LOSS_HALO_COLOR = new Color(1f, 0.25f, 0.25f);
    private static float HALO_ALPHA_MIN = 0.3f;
    private static float HALO_FLASH_RATE = 0.01f;
    private static float TIME_BETWEEN_FRAMES = 0.02f;
    private static float HALO_FADE_OUT_LIMIT = 0.1f;
    public static bool[] activeRows;
    private static bool stopContinuousFlashing = false;
    public static bool startFlashingNails = false;

    private static float normalSize = 6;
    private static float zoomedSize = 5.9f;

    private static int VICTORY_FLASH_ITERATIONS = 30;

    private static UnityEngine.UI.Text moveText;
    public static bool startFlashingMoves = false;
    private static int MAX_MOVE_TEXT_SIZE = 32;
    private static int NORMAl_MOVE_TEXT_SIZE = 24;
    private static Color NORMAL_TEXT_COLOR = new Color(0f, 0f, 0f);
    private static Color LOW_MOVES_TEXT_COLOR = new Color(1f, 0.25f, 0.25f);

    private static int SHAKE_COUNT = 5;
    private static float SHAKE_MAGNITUDE = 0.075f;
    private static Vector3 INITIAL_CAMERA_POSITION;

    private void Awake() {
        instance = this;
        victoryPanel = GameObject.Find("VictoryPanel");
        defeatPanel = GameObject.Find("DefeatPanel");
        moveText = GameObject.Find("MovesText").GetComponent<UnityEngine.UI.Text>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        INITIAL_CAMERA_POSITION = mainCamera.transform.localPosition;
        StartCoroutine(FlashMoveText());
        if (showHalos) {
            StartCoroutine(FlashHalos());
            StartCoroutine(FlashNailHalos());
        }
        DeactivateHaloArray();
        DeactivateNailHaloArray();
    }
    public static void ShakeScreen() {
        instance.StartCoroutine(instance.ShakeScreenCoroutine());
    }
    private IEnumerator ShakeScreenCoroutine() {
        int shakeCountdown = SHAKE_COUNT;
        while (shakeCountdown > 0) {
            mainCamera.transform.localPosition = INITIAL_CAMERA_POSITION + (Random.insideUnitSphere * SHAKE_MAGNITUDE);
            shakeCountdown -= 1;
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
        mainCamera.transform.localPosition = INITIAL_CAMERA_POSITION;
    }
    private IEnumerator FlashMoveText() {
        int currentTextSize = NORMAl_MOVE_TEXT_SIZE;
        int directionMultiplier = 1;
        while (true) {
            currentTextSize += directionMultiplier;
            if (currentTextSize >= MAX_MOVE_TEXT_SIZE) { directionMultiplier = -1; }
            else if (currentTextSize <= NORMAl_MOVE_TEXT_SIZE) { directionMultiplier = 1; }

            if (startFlashingMoves) {
                moveText.color = LOW_MOVES_TEXT_COLOR;
                moveText.fontSize = currentTextSize;
            } else {
                moveText.color = NORMAL_TEXT_COLOR;
                moveText.fontSize = NORMAl_MOVE_TEXT_SIZE;
            }
            yield return new WaitForSeconds(5f*TIME_BETWEEN_FRAMES);
        }
    }
    private IEnumerator FlashHalos() {
        float currentAlpha = 0f;
        float directionMultiplier = 1f;
        Color baseColor = NORMAL_HALO_COLOR;
        while (true) {
            if (!stopContinuousFlashing) {
                currentAlpha += directionMultiplier * HALO_FLASH_RATE;
                if (currentAlpha >= 1f) { directionMultiplier = -1f; } 
                else if (currentAlpha <= HALO_ALPHA_MIN) { directionMultiplier = 1f; }

                for (int row = 0; row < activeRows.Length; row++) {
                    if (activeRows[row]) { baseColor.a = currentAlpha; } 
                    else { baseColor.a = 0f; }
                    for (int haloIndex = 0; haloIndex < haloArray.GetLength(0); haloIndex++) {
                        haloArray[haloIndex, row].color = baseColor;
                    }
                }
            }
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
    }
    private IEnumerator FlashNailHalos() {
        float currentAlpha = 1f;
        float directionMultiplier = -1f;
        Color baseColor = SOLVED_HALO_COLOR;
        while (true) {
            if (startFlashingNails) {
                currentAlpha += directionMultiplier * HALO_FLASH_RATE * 5f/6f;
                if (currentAlpha >= 1f) { directionMultiplier = -1f; } 
                else if (currentAlpha <= HALO_ALPHA_MIN) { directionMultiplier = 1f; }
                baseColor.a = currentAlpha;
            } else {
                baseColor.a = 0f;
            }
            foreach (SpriteRenderer spriteRenderer in nailHaloArray) {
                spriteRenderer.color = baseColor;
            }
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
    }
    public static void PlayVictoryCreationAnimation(HaloStatus haloStatus) {
        instance.StartCoroutine(instance.VictoryCreationCoroutine(haloStatus));
    }
    public static void PlayVictoryAnimation(HaloStatus haloStatus) {
        instance.StartCoroutine(instance.VictoryCoroutine(haloStatus));
    }
    public static void PlayDefeatAnimation() {
        instance.StartCoroutine(instance.DefeatCoroutine());
    }
    private IEnumerator DefeatCoroutine() {
        stopContinuousFlashing = true;
        yield return StartCoroutine(HaloFadeOut());
        StartCoroutine(ZoomCamera());
        yield return StartCoroutine(HaloFlashVictory(HaloStatus.Loss));
        PanelManager.FadePartialDarken(true);
        PanelManager.FadePanel(defeatPanel, true);
    }
    private IEnumerator VictoryCreationCoroutine(HaloStatus haloStatus) {
        stopContinuousFlashing = true;
        yield return StartCoroutine(HaloFadeOut());
        StartCoroutine(ZoomCamera());
        StartCoroutine(HaloFlashVictory(haloStatus));
    }
    private IEnumerator VictoryCoroutine(HaloStatus haloStatus) {
        stopContinuousFlashing = true;
        yield return StartCoroutine(HaloFadeOut());
        StartCoroutine(ZoomCamera());
        yield return StartCoroutine(HaloFlashVictory(haloStatus));
        PanelManager.FadePartialDarken(true);
        PanelManager.FadePanel(victoryPanel, true);
    }
    private IEnumerator ZoomCamera() {
        float zoomRate = (normalSize - zoomedSize) / (float)VICTORY_FLASH_ITERATIONS;
        while (mainCamera.orthographicSize > zoomedSize) {
            mainCamera.orthographicSize -= zoomRate;
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
        while (mainCamera.orthographicSize < normalSize) {
            mainCamera.orthographicSize += zoomRate;
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
        mainCamera.orthographicSize = normalSize;
    }
    private IEnumerator HaloFadeOut() {
        float currentAlpha = haloArray[0, 0].color.a;
        float directionMultiplier = -1f;
        float flashRate = HALO_FLASH_RATE * 4f;

        Color baseColor = NORMAL_HALO_COLOR;
        while (currentAlpha > HALO_FADE_OUT_LIMIT) {
            currentAlpha += directionMultiplier * flashRate;
            baseColor.a = currentAlpha;
            foreach (SpriteRenderer renderer in haloArray) {
                renderer.color = baseColor;
            }
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
    }
    private IEnumerator HaloFlashVictory(HaloStatus haloStatus) {
        float currentAlpha = HALO_FADE_OUT_LIMIT;
        float flashRate = (1f- HALO_FADE_OUT_LIMIT)/(float)VICTORY_FLASH_ITERATIONS;
        Color baseColor = SOLVED_HALO_COLOR;
        switch (haloStatus) {
            case HaloStatus.Solved: baseColor = SOLVED_HALO_COLOR; break;
            case HaloStatus.Par: baseColor = PAR_HALO_COLOR; break;
            case HaloStatus.Loss: baseColor = LOSS_HALO_COLOR; break;
            default: break;
        }
        while (currentAlpha > 0) {
            currentAlpha += flashRate;
            baseColor.a = currentAlpha;
            foreach (SpriteRenderer renderer in haloArray) {
                renderer.color = baseColor;
            }
            if (currentAlpha >= 1f) { 
                flashRate = (-1f) / (float)VICTORY_FLASH_ITERATIONS; 
            }
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
    }

    public static void SetRowActiveStatus(int row, bool value) {
        if (activeRows == null) { return; }
        if (row < activeRows.Length && row >= 0) {
            activeRows[row] = value;
        }
    }
    public static void SetHaloPrefab(GameObject prefab) {
        HaloPrefab = prefab;
    }
    public static void SetNailHaloPrefab(GameObject prefab) {
        NailHaloPrefab = prefab;
    }
    public static void InitializeHaloArray(OrbBoard board) {
        OrbGoal[,] goals = board.orbGoals;
        OrbSlot[,] slots = board.orbSlots;
        int rows = goals.GetLength(1);
        int cols = goals.GetLength(0) + slots.GetLength(0);
        stopContinuousFlashing = false;
        ClearHaloArray();
        haloArray = new SpriteRenderer[cols, rows];
        activeRows = new bool[rows];
        for (int row = 0; row < rows; row++) {
            GameObject goalHalo1 = (GameObject)Instantiate(
                HaloPrefab, goals[0, row].transform.position, goals[0, row].transform.rotation);
            GameObject goalHalo2 = (GameObject)Instantiate(
                HaloPrefab, goals[1, row].transform.position, goals[1, row].transform.rotation);
            haloArray[0, row] = goalHalo1.GetComponent<SpriteRenderer>();
            haloArray[cols - 1, row] = goalHalo2.GetComponent<SpriteRenderer>();
            for (int col = 0; col < slots.GetLength(0); col++) {
                GameObject slotHalo = (GameObject)Instantiate(
                    HaloPrefab, slots[col, row].transform.position, slots[col, row].transform.rotation);
                haloArray[col + 1, row] = slotHalo.GetComponent<SpriteRenderer>();
            }
        }
        DeactivateHaloArray();
    }
    public static void InitializeNailHaloArray(OrbRotator rotator) {
        RotatorPin[,] pins = rotator.GetRotatorPins();
        startFlashingNails = false;
        ClearNailHaloArray();
        nailHaloArray = new SpriteRenderer[rotator.width * rotator.height];
        for (int row = 0; row < rotator.height; row++) {
            for (int col = 0; col < rotator.width; col++) {
                GameObject nailHalo = (GameObject)Instantiate(
                    NailHaloPrefab, pins[col, row].transform.position, pins[col, row].transform.rotation);
                nailHaloArray[row * rotator.width + col] = nailHalo.GetComponent<SpriteRenderer>();
            }
        }
        DeactivateNailHaloArray();
    }
    private static void DeactivateHaloArray() {
        Color baseColor = NORMAL_HALO_COLOR;
        baseColor.a = 0f;
        foreach (SpriteRenderer spriteRenderer in haloArray) {
            spriteRenderer.color = baseColor;
        }
    }
    private static void DeactivateNailHaloArray() {
        Color baseColor = SOLVED_HALO_COLOR;
        baseColor.a = 0f;
        foreach (SpriteRenderer spriteRenderer in nailHaloArray) {
            spriteRenderer.color = baseColor;
        }
    }
    private static void ClearHaloArray() {
        if (haloArray == null || haloArray.Length == 0) {
            return;
        }
        foreach (SpriteRenderer renderer in haloArray) {
            if (renderer != null) {
                Destroy(renderer.gameObject);
            }
        }
        haloArray = null;
    }
    private static void ClearNailHaloArray() {
        if (nailHaloArray == null || nailHaloArray.Length == 0) {
            return;
        }
        foreach (SpriteRenderer renderer in nailHaloArray) {
            if (renderer != null) {
                Destroy(renderer.gameObject);
            }
        }
        nailHaloArray = null;
    }
}
