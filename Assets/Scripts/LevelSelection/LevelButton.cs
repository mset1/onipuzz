﻿using UnityEngine;
using System.Collections;

public class LevelButton : MonoBehaviour {
    private BoardParameters parameters;
    public UnityEngine.UI.Image image;

    private void Awake() {
        Transform buttonObject = this.transform.Find("LevelButton");
        this.image = buttonObject.GetComponent<UnityEngine.UI.Image>();
    }
    public void SetParameters(BoardParameters parameters) {
        this.parameters = parameters;
        this.SetImage();
    }
    private void SetImage() {
        this.image.sprite = LevelWriter.GetSprite(this.parameters.id);
    }
    public BoardParameters GetBoardParameters() {
        return this.parameters;
    }
}
