﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PackSelector : MonoBehaviour {
    public GameObject PackButtonPrefab;
    private MainMenuHandler mainMenuHandler;
    private List<BoardParameters> parameters33 = new List<BoardParameters>();
    private List<BoardParameters> parameters43 = new List<BoardParameters>();
    private List<BoardParameters> parameters34 = new List<BoardParameters>();
    private List<BoardParameters> parameters44 = new List<BoardParameters>();
    private List<BoardParameters> parameters35 = new List<BoardParameters>();
    private List<BoardParameters> parameters45 = new List<BoardParameters>();
    private List<BoardParameters> parameters36 = new List<BoardParameters>();
    private List<BoardParameters> parameters46 = new List<BoardParameters>();

    private static int BUTTON_Y_OFFSET = -90;
    private static int BUTTON_HEIGHT = 40;

    public void InitializePacks() {
        this.LoadParameters(BoardParameters.Packs.Pack3x3, ref parameters33);
        this.LoadParameters(BoardParameters.Packs.Pack4x3, ref parameters43);
        this.LoadParameters(BoardParameters.Packs.Pack3x4, ref parameters34);
        this.LoadParameters(BoardParameters.Packs.Pack4x4, ref parameters44);
        this.LoadParameters(BoardParameters.Packs.Pack3x5, ref parameters35);
        this.LoadParameters(BoardParameters.Packs.Pack4x5, ref parameters45);
        //this.LoadParameters(3, 6, ref parameters36);

        this.LoadParameters(BoardParameters.Packs.Pack4x6, ref parameters46);
        InitializePackButtons();
    }
    public void SetMainMenuHandler(MainMenuHandler mainMenuHandler) {
        this.mainMenuHandler = mainMenuHandler;
    }
    private void InitializePackButtons() {
        this.CreateButton("3x3 Pack", parameters33, new Vector2(0f, BUTTON_Y_OFFSET - 0 * BUTTON_HEIGHT));
        this.CreateButton("4x3 Pack", parameters43, new Vector2(0f, BUTTON_Y_OFFSET - 1 * BUTTON_HEIGHT));
        this.CreateButton("3x4 Pack", parameters34, new Vector2(0f, BUTTON_Y_OFFSET - 2 * BUTTON_HEIGHT));
        this.CreateButton("4x4 Pack", parameters44, new Vector2(0f, BUTTON_Y_OFFSET - 3 * BUTTON_HEIGHT));
        this.CreateButton("3x5 Pack", parameters35, new Vector2(0f, BUTTON_Y_OFFSET - 4 * BUTTON_HEIGHT));
        this.CreateButton("4x5 Pack", parameters45, new Vector2(0f, BUTTON_Y_OFFSET - 5 * BUTTON_HEIGHT));
        this.CreateButton("4x6 Pack", parameters46, new Vector2(0f, BUTTON_Y_OFFSET - 6 * BUTTON_HEIGHT));
    }
    private void CreateButton(string text, List<BoardParameters> parametersList, Vector2 anchoredPosition) {
        GameObject buttonObject = (GameObject)Instantiate(PackButtonPrefab);
        buttonObject.transform.SetParent(this.transform, false);
        buttonObject.GetComponent<RectTransform>().anchoredPosition = anchoredPosition;

        PackButton packButton = buttonObject.GetComponent<PackButton>();
        packButton.SetParametersList(parametersList);

        UnityEngine.UI.Button button = buttonObject.GetComponent<UnityEngine.UI.Button>();
        button.onClick.AddListener(() => this.mainMenuHandler.SelectPack(packButton.GetParametersList()));
        
        Transform buttonTextTransform = buttonObject.transform.Find("Text");
        buttonTextTransform.GetComponent<UnityEngine.UI.Text>().text = text;

        Transform crownTransform = buttonObject.transform.Find("CrownImage");
        UnityEngine.UI.Image crownImage = crownTransform.GetComponent<UnityEngine.UI.Image>();

        if (parametersList.Count <= 0) { button.interactable = false; }
        bool isCrowned = CrownManager.SetCrownFromList(crownImage, parametersList);

        Transform crownBG = buttonObject.transform.Find("CrownBG");
        UnityEngine.UI.Image bgImage = crownBG.GetComponent<UnityEngine.UI.Image>();
        bgImage.enabled = isCrowned;

        Debug.Log(text + ": " + parametersList.Count + " boards");
    }
    private void LoadParameters(BoardParameters.Packs pack, ref List<BoardParameters> list) {
        foreach (BoardParameters parameters in LevelWriter.boardCollection.boards) {
            if (parameters.packId == pack) {
                list.Add(parameters);
            }
        }
        list = SortParametersByLevel(list);
    }
    private List<BoardParameters> SortParametersByLevel(List<BoardParameters> boards) {
        Debug.Log("Sorting through:" + boards.Count + " boards");
        List<BoardParameters> sortedBoards = new List<BoardParameters>();
        for (int i = 0; i < boards.Count; i++) {
            foreach (BoardParameters board in boards) {
                if (board.levelId == i) {
                    sortedBoards.Add(board);
                }
            }
        }
        return sortedBoards;
    }
}
