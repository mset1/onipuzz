﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenuHandler : MonoBehaviour {
    private PackSelector packSelector;
    private LevelSelector levelSelector;
    private GameObject levelPanel;
    private GameObject titlePanel;
    private GameObject currentPanel;
    private GameObject optionsPanel;
    private GameObject creditsPanel;

    public static UnityEngine.UI.Text statusText;

    private void Awake() {
        statusText = GameObject.Find("StatusText").GetComponent<UnityEngine.UI.Text>();
    }
    private void Start() {
        statusText.text = "Initiating Loading Sequence...";
        if (!LevelWriter.IsLoaded()) {
            LevelWriter.Load();
            Debug.Log("Loading, but should have been done in a loading scene");
        }
        StartCoroutine(LoadLevelSprites());

        PlayerRecordKeeper.Load();
        this.titlePanel = GameObject.Find("TitlePanel");
        this.optionsPanel = GameObject.Find("OptionsPanel");
        this.creditsPanel = GameObject.Find("CreditsPanel");

        this.levelSelector = GameObject.Find("LevelSelectPanel").GetComponent<LevelSelector>();
        this.levelSelector.SetMainMenuHandler(this);
        this.levelPanel = this.levelSelector.transform.parent.parent.gameObject;

        this.packSelector = GameObject.Find("PackSelectPanel").GetComponent<PackSelector>();
        this.packSelector.SetMainMenuHandler(this);
        this.packSelector.InitializePacks();

        this.currentPanel = titlePanel;
        this.LoadInitialMenu();
    }
    private IEnumerator LoadLevelSprites() {
        foreach (BoardParameters.Packs pack in System.Enum.GetValues(typeof(BoardParameters.Packs))) {
            Debug.Log(pack);
            yield return null;
        }
    }
    private void LoadInitialMenu() {
        switch (GameParameters.LOAD_PANEL) {
            case GameParameters.MenuPanel.PACK:
                InstantSwap(this.packSelector.gameObject);
                break;
            case GameParameters.MenuPanel.LEVEL:
                InstantSwap(this.levelPanel);
                this.levelSelector.CreateLevelButtonGrid(GameParameters.levelPack);
                break;
        }
    }
    public void LoadTutorial() {
        BoardParameters tutorialBoard = LevelWriter.GetBoardParameters(BoardParameters.Packs.Tutorial, 0);
        Debug.Log("Tutorial Level: " + tutorialBoard.id);
        GameParameters.boardParameters = tutorialBoard.clone();
        LoadGameScene(GameParameters.TUTORIAL_SCENE);
        AudioManager.PlayButtonSound();
    }
    public void SelectPack(List<BoardParameters> parametersList) {
        if (PanelManager.IsActiveBlackScreen()) { return; }
        this.levelSelector.CreateLevelButtonGrid(parametersList);
        GameParameters.levelPack = parametersList;
        PanelManager.FadeTranstion(this.currentPanel, levelPanel.gameObject);
        this.currentPanel = levelPanel.gameObject;
        statusText.text = "Going to Level Select";
        AudioManager.PlayButtonSound();
    }
    public void FadeToPackSelect() {
        if (PanelManager.IsActiveBlackScreen()) { return; }
        PanelManager.FadeTranstion(this.currentPanel, packSelector.gameObject);
        this.currentPanel = packSelector.gameObject;
        statusText.text = "Going to Pack Select";
        AudioManager.PlayButtonSound();
    }
    public void FadeToTitle() {
        if (PanelManager.IsActiveBlackScreen()) { return; }
        PanelManager.FadeTranstion(this.currentPanel, this.titlePanel);
        this.currentPanel = this.titlePanel;
        statusText.text = "Going to Title";
        AudioManager.PlayButtonSound();
    }
    public void FadeToOptions() {
        if (PanelManager.IsActiveBlackScreen()) { return; }
        PanelManager.FadeTranstion(this.currentPanel, this.optionsPanel);
        this.currentPanel = this.optionsPanel;
        statusText.text = "Going to Options";
        AudioManager.PlayButtonSound();
    }
    public void FadeToCredits() {
        if (PanelManager.IsActiveBlackScreen()) { return; }
        PanelManager.FadeTranstion(this.currentPanel, this.creditsPanel);
        this.currentPanel = this.creditsPanel;
        statusText.text = "Going to Credits";
        AudioManager.PlayButtonSound();
    }
    public void LoadGameScene(int sceneId) {
        statusText.text = "Starting Game";
        if (PanelManager.IsActiveBlackScreen()) { return; }
        PanelManager.FadePanel(this.currentPanel, false);
        PanelManager.FadeLoad(sceneId);
        AudioManager.PlayButtonSound();
    }
    private void InstantSwap(GameObject panelNew) {
        PanelManager.InstantTransition(this.currentPanel, panelNew);
        this.currentPanel = panelNew;
    }
}
