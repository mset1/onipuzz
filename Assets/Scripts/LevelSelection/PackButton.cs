﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PackButton : MonoBehaviour {
    private List<BoardParameters> parametersList;

    public void SetParametersList(List<BoardParameters> parameters) {
        this.parametersList = parameters;
    }
    public List<BoardParameters> GetParametersList() {
        return this.parametersList;
    }
}
