﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GameParameters {
    public static PlayerRecord playerRecord;
    public static BoardParameters boardParameters;
    public static List<BoardParameters> levelPack;
    public static int LOADING_SCENE = 0;
    public static int LEVEL_SELECT_SCENE = 1;
    public static int GAME_SCENE = 2;
    public static int TUTORIAL_SCENE = 3;
    public static int SANDBOX_SCENE = 4;
    public static int CREATION_SCENE = 4;
    public static MenuPanel LOAD_PANEL = MenuPanel.TITLE;

    public enum MenuPanel {
        TITLE, PACK, LEVEL
    }
    public static BoardParameters GetNextBoardFromPack(BoardParameters parameters) {
        int parametersIndex = -1;
        for (int i = 0; i < levelPack.Count; i++) {
            if (levelPack[i].id == parameters.id) { parametersIndex = i; }
        }
        if (parametersIndex < levelPack.Count - 1 && parametersIndex >= 0) {
            return levelPack[parametersIndex + 1];
        }
        return null;
    }
}
