﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PanelManager : MonoBehaviour {
    private static PanelManager instance;
    public static Vector2 ONSCREEN_CENTER = new Vector2(0f, 0f);
    public static Vector2 OFFSCREEN_R = new Vector2(2000f, 0f);
    public static Vector2 OFFSCREEN_D = new Vector2(0f, -2000f);
    private static float FADE_RATE = 0.1f;
    public static GameObject BLACK_SCREEN;
    public static float BLACKOUT_DELAY = 0.5f;
    private static float TIME_BETWEEN_FRAMES = 0.02f;

    private void Awake() {
        instance = this;
        BLACK_SCREEN = GameObject.Find("BlackScreen");
        StartCoroutine(instance.FadeIntoScene());
    }
    public static void FadePanel(GameObject panel, bool isEnter) {
        if (instance == null) { return; }
        instance.CoroutineFadePanel(panel, isEnter);
    }
    public static void FadePartialDarken(bool isEnter) {
        if (instance == null) { return; }
        instance.CoroutinePartialDarken(isEnter);        
    }
    public static void FadeTranstion(GameObject panelOld, GameObject panelNew) {
        if (instance == null) { return; }
        instance.InitializeFadeTransition(panelOld, panelNew);
    }
    public static void FadeLoad(int sceneId) {
        if (instance == null) { return; }
        instance.InitializeFadeLoad(sceneId);
    }
    public static void InstantTransition(GameObject panelOld, GameObject panelNew) {
        if (instance == null) { return; }
        RectTransform rtOld = panelOld.GetComponent<RectTransform>();
        rtOld.anchoredPosition = OFFSCREEN_D;
        panelOld.GetComponent<CanvasGroup>().alpha = 0;
        RectTransform rtNew = panelNew.GetComponent<RectTransform>();
        rtNew.anchoredPosition = ONSCREEN_CENTER;
        panelNew.GetComponent<CanvasGroup>().alpha = 1;
    }
    public static void Blackout() {
        if (instance == null) { return; }
        instance.InitializeBlackout();
    }
    private void InitializeBlackout() {
        StartCoroutine(instance.BlackoutCoroutine());
    }
    private void CoroutinePartialDarken(bool isEnter) {
        float fadeRate = FADE_RATE;
        Vector2 targetLocation = ONSCREEN_CENTER;
        if (!isEnter) {
            fadeRate *= -1;
            targetLocation = OFFSCREEN_D;
        }
        StartCoroutine(
            instance.FadeRoutine(fadeRate, BLACK_SCREEN, targetLocation, 0f, 0.5f));

    }
    private void CoroutineFadePanel(GameObject panel, bool isEnter) {
        float fadeRate = FADE_RATE;
        Vector2 targetLocation = ONSCREEN_CENTER;
        if (!isEnter) { 
            fadeRate *= -1;
            targetLocation = OFFSCREEN_D;
        }
        StartCoroutine(FadeRoutine(fadeRate, panel, targetLocation));
    }
    private void InitializeFadeTransition(GameObject panelOld, GameObject panelNew) {
        StartCoroutine(instance.CoroutineFadeTransition(panelOld, panelNew));
    }
    private IEnumerator CoroutineFadeTransition(GameObject panelOld, GameObject panelNew) {
        yield return StartCoroutine(FadeRoutine(-FADE_RATE, panelOld, OFFSCREEN_D));
        yield return StartCoroutine(FadeRoutine(FADE_RATE, panelNew, ONSCREEN_CENTER));
    }
    private IEnumerator FadeRoutine(float fadeRate, GameObject panel, Vector2 targetLocation) {
        yield return StartCoroutine(FadeRoutine(fadeRate, panel, targetLocation, 0f, 1f));
    }
    private IEnumerator FadeRoutine(float fadeRate, GameObject panel, Vector2 targetLocation, float alphaMin, float alphaMax) {
        if (fadeRate > 0) {
            RectTransform rt = panel.GetComponent<RectTransform>();
            rt.anchoredPosition = targetLocation;
        }
        CanvasGroup group = panel.GetComponent<CanvasGroup>();
        group.interactable = false;
        float currentAlpha = group.alpha;
        while (currentAlpha >= alphaMin && currentAlpha <= alphaMax) {
            currentAlpha += fadeRate;
            group.alpha = currentAlpha;
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
        if (fadeRate < 0) {
            RectTransform rt = panel.GetComponent<RectTransform>();
            rt.anchoredPosition = targetLocation;
        }
        group.interactable = true;
    }
    private void InitializeFadeLoad(int sceneId) {
        StartCoroutine(instance.FadeLoadScene(sceneId));
    }
    private IEnumerator FadeIntoScene() {
        RectTransform rt = BLACK_SCREEN.GetComponent<RectTransform>();
        rt.anchoredPosition = ONSCREEN_CENTER;
        CanvasGroup group = BLACK_SCREEN.GetComponent<CanvasGroup>();
        float currentAlpha = 1f;
        group.alpha = currentAlpha;
        while (currentAlpha > 0) {
            currentAlpha -= FADE_RATE/3;
            group.alpha = currentAlpha;
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
        rt.anchoredPosition = OFFSCREEN_D;
    }
    private IEnumerator FadeLoadScene(int sceneId) {
        RectTransform rt = BLACK_SCREEN.GetComponent<RectTransform>();
        rt.anchoredPosition = ONSCREEN_CENTER;
        CanvasGroup group = BLACK_SCREEN.GetComponent<CanvasGroup>();
        float currentAlpha = 1f;
        group.alpha = currentAlpha;
        while (currentAlpha < 1) {
            currentAlpha += FADE_RATE/3;
            group.alpha = currentAlpha;
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
        SceneManager.LoadScene(sceneId);
    }
    private IEnumerator BlackoutCoroutine() {
        RectTransform rt = BLACK_SCREEN.GetComponent<RectTransform>();
        rt.anchoredPosition = ONSCREEN_CENTER;
        CanvasGroup group = BLACK_SCREEN.GetComponent<CanvasGroup>();
        float currentTime = Time.time;
        float currentAlpha = 1f;
        group.alpha = currentAlpha;
        while (currentAlpha < 1f) {
            currentAlpha += FADE_RATE/2;
            group.alpha = currentAlpha;
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
        float elapsedTime = Time.time - currentTime;
        float waitTime = BLACKOUT_DELAY - (2 * elapsedTime);
        yield return new WaitForSeconds(waitTime);
        while (currentAlpha > 0) {
            currentAlpha -= FADE_RATE/2;
            group.alpha = currentAlpha;
            yield return new WaitForSeconds(TIME_BETWEEN_FRAMES);
        }
        rt.anchoredPosition = OFFSCREEN_D;
    }
    public static bool IsActiveBlackScreen() {
        RectTransform rt = PanelManager.BLACK_SCREEN.GetComponent<RectTransform>();
        return rt.anchoredPosition == PanelManager.ONSCREEN_CENTER;
    }
}
