﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class GameLoader : MonoBehaviour {
    public UnityEngine.UI.Image progressBar;
    public UnityEngine.UI.Text debugText;

    public static int IMAGES_LOADED = 0;
    public static int TOTAL_IMAGES = 0;
    public bool isLevelCreator = false;

    private void Start() {
        LevelWriter.ReadBoardsFromJSON();
        TOTAL_IMAGES = LevelWriter.boardCollection.boards.Count;
        StartCoroutine(UpdateProgressBar());
        StartCoroutine(LoadSpritesCoroutine());
    }
    private IEnumerator UpdateProgressBar() {
        while (IMAGES_LOADED < TOTAL_IMAGES) {
            float percentage = ((float)IMAGES_LOADED) / ((float)TOTAL_IMAGES);
            progressBar.fillAmount = percentage;
            debugText.text = "" + (int)(percentage * 100) + " %";
            yield return null;
        }
        debugText.text = "100 %";
        if (!this.isLevelCreator) {
            PanelManager.FadeLoad(GameParameters.LEVEL_SELECT_SCENE);
        } else {
            PanelManager.FadeLoad(GameParameters.CREATION_SCENE);
        }
    }
    private IEnumerator LoadSpritesCoroutine() {
        while (IMAGES_LOADED < TOTAL_IMAGES) {
            string filename = LevelWriter.SAVE_DIRECTORY + LevelWriter.IMAGE_SUBDIR + LevelWriter.GetImageName(IMAGES_LOADED);
            byte[] pngBytes = null;
            if (Application.platform == RuntimePlatform.Android) {
                UnityWebRequest loadingRequest = LevelWriter.LoadStreamingAssetFromAndroid(filename);
                if (loadingRequest != null) {
                    pngBytes = loadingRequest.downloadHandler.data;
                }
                GameLoader.IMAGES_LOADED += 1;
            } else {
                pngBytes = System.IO.File.ReadAllBytes(filename);
                GameLoader.IMAGES_LOADED += 1;
            }
            if (pngBytes != null) {
                Texture2D testTexture = new Texture2D(120, 120);
                testTexture.LoadImage(pngBytes);
                Sprite sprite = Sprite.Create(testTexture, new Rect(0, 0, testTexture.width, testTexture.height), new Vector2(0.5f, 0.5f));
                LevelWriter.boardSprites.Add(sprite);
            }
            yield return null;
        }
    }
    public IEnumerator LoadSpritesFromPack(BoardParameters.Packs pack) {
        List<BoardParameters> boards = GetBoardsFromPack(pack);
        int imagesLoadedFromPack = 0;
        while (imagesLoadedFromPack < boards.Count) {
            string filename = LevelWriter.SAVE_DIRECTORY + LevelWriter.IMAGE_SUBDIR + LevelWriter.GetImageName(boards[imagesLoadedFromPack]);
            byte[] pngBytes = LoadBytesFromFile(filename);
            imagesLoadedFromPack += 1;
            GameLoader.IMAGES_LOADED += 1;
            AddSpriteToLevelWriter(pngBytes);
            yield return null;
        }
    }
    private byte[] LoadBytesFromFile(string filename) {
        byte[] pngBytes = null;
        if (Application.platform == RuntimePlatform.Android) {
            UnityWebRequest loadingRequest = LevelWriter.LoadStreamingAssetFromAndroid(filename);
            if (loadingRequest != null) {
                pngBytes = loadingRequest.downloadHandler.data;
            }
        } else {
            pngBytes = System.IO.File.ReadAllBytes(filename);
        }
        return pngBytes;
    }
    private void AddSpriteToLevelWriter(byte[] pngBytes) {
        if (pngBytes != null) {
            Texture2D testTexture = new Texture2D(120, 120);
            testTexture.LoadImage(pngBytes);
            Sprite sprite = Sprite.Create(testTexture, new Rect(0, 0, testTexture.width, testTexture.height), new Vector2(0.5f, 0.5f));
            LevelWriter.boardSprites.Add(sprite);
        }
    }
    private List<BoardParameters> GetBoardsFromPack(BoardParameters.Packs pack) {
        List<BoardParameters> list = new List<BoardParameters>();
        foreach (BoardParameters parameters in LevelWriter.boardCollection.boards) {
            if (parameters.packId == pack) {
                list.Add(parameters);
            }
        }
        return list;
    }
}
