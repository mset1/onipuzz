﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour {
    public PackSelector packSelector;
    private MainMenuHandler mainMenuHandler;
    public GameObject LevelButtonPrefab;
    public LevelButton[] levels;
    private static float PANEL_WIDTH = 120;
    private static float PANEL_HEIGHT = 140;

    void Awake() {
        InitializePanelDimensions();
    }
    public void SelectLevel(BoardParameters parameters) {
        Debug.Log("Selecting board Id:" + parameters.id);
        GameParameters.boardParameters = parameters.clone();
        mainMenuHandler.LoadGameScene(GameParameters.GAME_SCENE);
    }
    public void SetMainMenuHandler(MainMenuHandler mainMenuHandler) {
        this.mainMenuHandler = mainMenuHandler;
    }
    private void InitializePanelDimensions() {
        RectTransform rTransform = LevelButtonPrefab.GetComponent<RectTransform>();
        PANEL_WIDTH = rTransform.sizeDelta.x;
        PANEL_HEIGHT = rTransform.sizeDelta.y;
    }
    public void CreateLevelButtonGrid(List<BoardParameters> parametersList) {
        this.ClearLevelButtonGrid();
        RectTransform rTransform = this.GetComponent<RectTransform>();
        int cols = (int)(rTransform.sizeDelta.x / PANEL_WIDTH);
        float xOffset = -(cols / 2f) + 0.5f;

        this.ResizePanel(((parametersList.Count-1) / cols) + 1);

        this.levels = new LevelButton[parametersList.Count];
        for (int i = 0; i < parametersList.Count; i++) {
            int xMult = i % cols;
            int yMult = -i / cols;
            float xPos = (i % cols + xOffset) * PANEL_WIDTH;
            float yPos = (-i / cols) * PANEL_HEIGHT;
            Vector2 anchoredPosition = new Vector2(xPos, yPos);
            this.levels[i] = this.CreateLevelButton(parametersList[i], anchoredPosition);
            Transform levelText = this.levels[i].transform.Find("LevelText");
            levelText.GetComponent<UnityEngine.UI.Text>().text = "Level " + (i + 1);
        }
    }
    private void ResizePanel(int rows) {
        RectTransform rt = this.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(rt.sizeDelta.x, PANEL_HEIGHT * rows);
    }
    private LevelButton CreateLevelButton(BoardParameters parameters, Vector2 anchoredPosition) {
        GameObject levelObject = (GameObject)Instantiate(LevelButtonPrefab);
        levelObject.transform.SetParent(this.transform, false);
        RectTransform levelRT = levelObject.GetComponent<RectTransform>();
        levelRT.anchoredPosition = anchoredPosition;

        LevelButton levelButton = levelObject.GetComponent<LevelButton>();
        levelButton.SetParameters(parameters);
        Transform buttonTransform = levelObject.transform.Find("LevelButton");
        UnityEngine.UI.Button button = buttonTransform.GetComponent<UnityEngine.UI.Button>();
        button.onClick.AddListener(() => this.SelectLevel(levelButton.GetBoardParameters()));

        Transform crownTransform = levelObject.transform.Find("CrownImage");
        UnityEngine.UI.Image crownImage = crownTransform.GetComponent<UnityEngine.UI.Image>();
        bool isCrowned = CrownManager.SetCrownImage(crownImage, parameters);

        Transform crownBG = levelObject.transform.Find("CrownBG");
        UnityEngine.UI.Image bgImage = crownBG.GetComponent<UnityEngine.UI.Image>();
        bgImage.enabled = isCrowned;

        Transform levelText = levelButton.transform.Find("LevelText");
        //levelText.GetComponent<UnityEngine.UI.Text>().text = "ID(par): " + parameters.id + " (" + parameters.parMoves + ")";
        //levelText.GetComponent<UnityEngine.UI.Text>().text = "ID(Lv): " + parameters.id + " (" + parameters.levelId + ")";

        return levelButton;
    }
    public void ClearLevelButtonGrid() {
        foreach (LevelButton button in this.levels) {
            Destroy(button.gameObject);
        }
    }
}
