﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class OniPuzzleTutorial : OniPuzzleGame {
    public GameObject ScoreMasker;
    public GameObject RowMasker;
    public GameObject PinMasker;
    public GameObject RotateMasker;
    public GameObject currentMasker;

    public int stepCounter = 0;
    private bool isSelectingUI = false;
    private EventSystem eventSystem;
    private const int SCORE_STEP1 = 0;
    private const int ROWMATCH_STEP = 1;
    private const int PIN_STEP = 2;
    private const int ROTATE_STEP1 = 3;
    private const int SCORE_STEP2 = 4;
    private const int ROTATE_STEP2 = 5;
    private const int DESIRED_PIN_X = 1;
    private const int DESIRED_PIN_Y = 1;

    private void Awake() {
        this.SetupUIElements();
        base.SetupKeys();
        base.InitializeBoardGenerator();
        this.currentParameters = GameParameters.boardParameters;
        base.InitializeBoard();
    }
    protected override void SetupUIElements() {
        base.victoryPanel = GameObject.Find("VictoryPanel");
        base.defeatPanel = GameObject.Find("DefeatPanel");
        base.menuPanel = GameObject.Find("MenuPanel");
        base.parText = GameObject.Find("ParText").GetComponent<UnityEngine.UI.Text>();
        base.moveText = GameObject.Find("MovesText").GetComponent<UnityEngine.UI.Text>();

        base.CCWButton = GameObject.Find("RotateCCWButton").GetComponent<UnityEngine.UI.Button>();
        base.CWButton = GameObject.Find("RotateCWButton").GetComponent<UnityEngine.UI.Button>();


        eventSystem = FindObjectOfType<EventSystem>();
        this.ScoreMasker = GameObject.Find("ScoreMasker");
        this.RowMasker = GameObject.Find("RowMasker");
        this.PinMasker = GameObject.Find("PinMasker");
        this.RotateMasker = GameObject.Find("RotateMasker");
        this.currentMasker = ScoreMasker;
    }
    protected override void Update() {
        if (Input.GetMouseButtonUp(0) && !isSelectingUI) {
            switch (stepCounter) {
                case SCORE_STEP1:
                    Debug.Log("Setting Row Mask");
                    SetMasker(RowMasker);
                    stepCounter += 1;
                    break;
                case ROWMATCH_STEP:
                    Debug.Log("Setting Pin Mask");
                    SetMasker(PinMasker);
                    stepCounter += 1;
                    break;
                case PIN_STEP:
                    if (CheckPinSelection()) {
                        SetMasker(RotateMasker);
                        stepCounter += 1;
                    }
                    break;
                case SCORE_STEP2:
                    SetMasker(RotateMasker);
                    stepCounter += 1;
                    break;
                default:
                    break;
            } 
        }
        isSelectingUI = false;
    }
    public void ReturnToMainMenu() {
        AudioManager.PlayButtonSound();
        GameParameters.LOAD_PANEL = GameParameters.MenuPanel.TITLE;
        PanelManager.FadePanel(this.menuPanel, false);
        PanelManager.FadePanel(this.victoryPanel, false);
        PanelManager.FadePanel(this.defeatPanel, false);
        PanelManager.FadeLoad(GameParameters.LEVEL_SELECT_SCENE);
    }
    public override void ResetBoard() {
        base.ResetBoard();
        this.stepCounter = 0;
        this.isSelectingUI = true;
        SetMasker(ScoreMasker);
    }
    public void TriggerRotateTutorial() {
        if (stepCounter == ROTATE_STEP1) {
            SetMasker(ScoreMasker);
            stepCounter += 1;
            isSelectingUI = true;
        } else if (stepCounter == ROTATE_STEP2) {
            stepCounter += 1;
            isSelectingUI = true;
        }
    }
    protected override void OnVictory() {
        SetMasker(null);
        RotatorController controller = this.currentBoard.rotator.GetComponent<RotatorController>();
        controller.isActive = false;
        EffectsHandler.HaloStatus haloStatus = EffectsHandler.HaloStatus.Solved;
        if (currentMoves <= this.currentParameters.parMoves) {
            haloStatus = EffectsHandler.HaloStatus.Par;
            AudioManager.PlayPerfectSound();
        } else {
            AudioManager.PlayVictorySound();
        }
        EffectsHandler.PlayVictoryAnimation(haloStatus);
    }
    protected override void OnReset() {
        base.OnReset();
        this.CWButton.onClick.AddListener(() => this.TriggerRotateTutorial());
    }
    private void SetMasker(GameObject masker) {
        if (this.currentMasker != null) {
            RectTransform rtOld = this.currentMasker.GetComponent<RectTransform>();
            rtOld.anchoredPosition = PanelManager.OFFSCREEN_D;
        }

        this.currentMasker = masker;
        if (masker == null) { return; }
        RectTransform rtNew = masker.GetComponent<RectTransform>();
        rtNew.anchoredPosition = PanelManager.ONSCREEN_CENTER;
    }
    private bool CheckPinSelection() {
        if (eventSystem != null && eventSystem.IsPointerOverGameObject()) {
            return false;
        }
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null) {
            RotatorPin[,] pins = this.currentBoard.rotator.GetRotatorPins();
            if (pins[DESIRED_PIN_X, DESIRED_PIN_Y].pinCollider == hit.collider) {
                return true;
            }
        }
        return false;
    }
}
