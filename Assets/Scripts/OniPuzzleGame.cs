﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class OniPuzzleGame : MonoBehaviour {
    private KeyCode reset_key { get; set; }

    public GameObject HaloPrefab;
    public GameObject NailHaloPrefab;
    public GameObject OrbBoardPrefab;
    protected OrbBoard currentBoard;
    protected RotatorController controller;
    public BoardParameters currentParameters;
    private BoardParameters nextParameters;
    protected int currentMoves = 0;
    public bool isWaitingOnRotate = false;

    protected UnityEngine.UI.Text moveText;
    protected UnityEngine.UI.Text parText;
    protected GameObject victoryPanel;
    protected GameObject defeatPanel;
    protected UnityEngine.UI.Button nextLevelButton;
    public GameObject menuPanel;
    public GameObject optionsPanel;
    protected UnityEngine.UI.Button CCWButton;
    protected UnityEngine.UI.Button CWButton;

    private void Awake() {
        this.SetupUIElements();
        this.InitializeBoardGenerator();
        this.currentParameters = GameParameters.boardParameters;
        this.InitializeBoard();
        this.SetupKeys();
    }
    protected virtual void Update() {
        if (Input.GetKeyUp(reset_key)) {
            ResetBoard();
        }
    }
    protected virtual void SetupKeys() {
        string reset_str = this.gameObject.name + "reset_key";
        reset_key = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(reset_str, KeyCode.C.ToString()));
    }
    protected virtual void SetupUIElements() {
        this.victoryPanel = GameObject.Find("VictoryPanel");
        this.defeatPanel = GameObject.Find("DefeatPanel");
        this.nextLevelButton = this.victoryPanel.transform.Find("VictoryNextButton").GetComponent<UnityEngine.UI.Button>();
        this.menuPanel = GameObject.Find("MenuPanel");
        this.optionsPanel = GameObject.Find("OptionsPanel");
        this.parText = GameObject.Find("ParText").GetComponent<UnityEngine.UI.Text>();
        this.moveText = GameObject.Find("MovesText").GetComponent<UnityEngine.UI.Text>();

        this.CCWButton = GameObject.Find("RotateCCWButton").GetComponent<UnityEngine.UI.Button>();
        this.CWButton = GameObject.Find("RotateCWButton").GetComponent<UnityEngine.UI.Button>();
    }
    protected void InitializeBoardGenerator() {
        BoardGenerator.SetGame(this);
        BoardGenerator.SetOrbBoardPrefab(this.OrbBoardPrefab);
    }
    protected virtual void InitializeBoard() {
        if (this.currentBoard != null) { Destroy(this.currentBoard.gameObject); }
        this.currentBoard = BoardGenerator.LoadBoard(this.currentParameters);
        this.parText.text = "" + this.currentParameters.parMoves;
        this.InitializeEffects();
        this.currentBoard.CheckBoardMatch();
        this.OnReset();
    }
    protected virtual void InitializeEffects() {
        if (HaloPrefab == null) { return; }
        EffectsHandler.SetHaloPrefab(HaloPrefab);
        EffectsHandler.SetNailHaloPrefab(NailHaloPrefab);
        EffectsHandler.InitializeHaloArray(this.currentBoard);
        EffectsHandler.InitializeNailHaloArray(this.currentBoard.rotator);
    }
    public virtual void ResetBoard() {
        AudioManager.PlayButtonSound();
        if (this.currentParameters == null || this.currentBoard == null) { return; }
        PanelManager.FadePanel(this.menuPanel, false);
        PanelManager.FadePanel(this.victoryPanel, false);
        PanelManager.FadePanel(this.defeatPanel, false);
        PanelManager.Blackout();
        StartCoroutine(InitializeWaitCall(this.InitializeBoard, PanelManager.BLACKOUT_DELAY / 2));
    }
    public void ReturnToLevelSelect() {
        AudioManager.PlayButtonSound();
        GameParameters.LOAD_PANEL = GameParameters.MenuPanel.LEVEL;
        PanelManager.FadePanel(this.menuPanel, false);
        PanelManager.FadePanel(this.victoryPanel, false);
        PanelManager.FadePanel(this.defeatPanel, false);
        PanelManager.FadeLoad(GameParameters.LEVEL_SELECT_SCENE);
    }
    public void OpenMenu() {
        PanelManager.FadePartialDarken(true);
        PanelManager.FadePanel(this.menuPanel, true);
        this.controller.isActive = false;
        AudioManager.PlayButtonSound();
    }
    public void CloseMenu() {
        PanelManager.FadePartialDarken(false);
        PanelManager.FadePanel(this.menuPanel, false);
        this.controller.isActive = true;
        AudioManager.PlayButtonSound();
    }
    public void OpenOptionsMenu() {
        PanelManager.FadePanel(this.menuPanel, false);
        PanelManager.FadePanel(this.optionsPanel, true);
        AudioManager.PlayButtonSound();
    }
    public void CloseOptionsMenu() {
        PanelManager.FadePanel(this.menuPanel, true);
        PanelManager.FadePanel(this.optionsPanel, false);
        AudioManager.PlayButtonSound();
    }
    public void IncrementMoves(int value) {
        this.isWaitingOnRotate = false;
        this.currentMoves += 1;
        this.moveText.text = "" + this.currentMoves;
        if (this.currentBoard.CheckBoardMatch()) {
            this.OnVictory();
        }
    }
    public virtual void IncrementLevel() {
        PanelManager.FadePanel(this.victoryPanel, false);
        PanelManager.Blackout();
        this.currentParameters = this.nextParameters;
        this.ResetBoard();
    }
    protected virtual void OnDefeat() {
        this.controller.isActive = false;
        EffectsHandler.PlayDefeatAnimation();
    }
    protected virtual void OnVictory() {
        this.nextParameters = GameParameters.GetNextBoardFromPack(this.currentParameters);
        if (this.nextParameters == null) {
            this.nextLevelButton.interactable = false;
        }
        else { this.nextLevelButton.enabled = true; }

        PlayerRecord playerRecord = new PlayerRecord();
        playerRecord.boardId = this.currentBoard.parameters.id;
        playerRecord.bestMoves = this.currentMoves;
        PlayerRecordKeeper.Save(playerRecord);
        this.controller.isActive = false;

        EffectsHandler.HaloStatus haloStatus = EffectsHandler.HaloStatus.Solved;
        if (currentMoves <= this.currentParameters.parMoves) {
            haloStatus = EffectsHandler.HaloStatus.Par;
            AudioManager.PlayPerfectSound();
        } else {
            AudioManager.PlayVictorySound();
        }
        EffectsHandler.PlayVictoryAnimation(haloStatus);

        Debug.Log("Par(Score):" + LevelWriter.GetBoardParameters(this.currentBoard.parameters.id).parMoves + "(" + this.currentMoves + ")");
        if (LevelWriter.GetBoardParameters(this.currentBoard.parameters.id).parMoves == 0 ||
            this.currentMoves < LevelWriter.GetBoardParameters(this.currentBoard.parameters.id).parMoves) {
            BoardParameters newParameters = this.currentBoard.parameters.clone();
            newParameters.parMoves = this.currentMoves;
            LevelWriter.Save(newParameters, false);
        }
    }
    protected virtual void OnReset() {
        this.controller = this.currentBoard.rotator.GetComponent<RotatorController>();
        this.controller.isActive = true;
        this.isWaitingOnRotate = false;
        this.ResetMoves();
        this.CCWButton.onClick.AddListener(() => this.OnRotate(false));
        this.CWButton.onClick.AddListener(() => this.OnRotate(true));
    }
    protected void ResetMoves() {
        this.currentMoves = 0;
        this.moveText.text = "" + this.currentMoves;
        EffectsHandler.startFlashingMoves = false;
    }
    private delegate void ActionDelegate();
    private IEnumerator InitializeWaitCall(ActionDelegate method, float seconds) {
        yield return new WaitForSeconds(seconds);
        method();
    }
    public virtual void OnRotate(bool isCW) {
        if (isWaitingOnRotate) { return; }
        this.controller.Rotate(isCW);
        isWaitingOnRotate = true;
    }
}
